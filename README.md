# README

This app provides the latest stock market information, as well as physical currency and cryptocurrency rates.

Data is fetched from the [Alpha Vantage](https://www.alphavantage.co) API and displayed on graphs using the _Chart.js_ library. Users can choose which stocks and currencies to follow. Some technical indicators data and graphs are also provided. Finally, there are some graph options that can be customized.

## App Information

App Name: financial-dashboard

Created: October 2019

Creator: Christian McTighe

Email: mctighecw@gmail.com

Repository: [Link](https://gitlab.com/mctighecw/financial-dashboard)

Production: [Link](https://financial.mctighecw.site)

## Tech Stack

Frontend:

- React (with React hooks)
- Redux
- Chart.js library for React
- Sass (scss)
- CSS Modules
- Webpack
- Prettier

Backend:

- Flask
- MongoDB
- JWT Authentication
- REST APIs
- Alpha Vantage Python package

## To Run

### From terminal

_Frontend_

- Setup:

  ```
  $ yarn install
  ```

- Start:

  ```
  $ yarn start
  ```

_Backend_

- Add `.env` file to root directory

- Setup:

  _Note_: Python 3.6 recommended

  ```
  $ cd backend
  $ virtualenv venv
  $ source venv/bin/activate
  (venv) $ pip install -r requirements.txt
  ```

- Initialize db:

  ```
  $ cd backend
  $ source venv/bin/activate
  (venv) $ FLASK_ENV='development' python src/db/init_db.py
  (set up admin user and password)
  ```

- If necessary, drop db (later on):

  ```
  $ cd backend
  $ source venv/bin/activate
  (venv) $ python src/db/drop_dev_db.py
  ```

- Start:

  ```
  $ cd backend
  $ source run_dev.sh
  ```

### With Docker

```
$ docker-compose up -d --build
$ docker exec -it financial-dashboard_mongodb_1 bash
  (set up admin user and password)
$ source init_docker_db.sh
```

Project running at `localhost:80`

## Credits

- Dashboard (BTC) icon courtesy of Eucalyp at [Flaticon](https://www.flaticon.com/authors/eucalyp)
- Dashboard control icon courtesy of Dave Gandy at [Flaticon](https://www.flaticon.com/authors/dave-gandy)
- Currency and bar chart with arrow icons courtesy of Gregor Cresnar at [Flaticon](https://www.flaticon.com/authors/gregor-cresnar)
- Magnifying glass icon courtesy of Pixel perfect at [Flaticon](https://www.flaticon.com/authors/pixel-perfect)
- All other icons courtesy of Freepik at [Flaticon](https://www.flaticon.com/authors/freepik)

Last updated: 2024-12-23
