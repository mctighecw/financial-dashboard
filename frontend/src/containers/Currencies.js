import { connect } from 'react-redux';
import Currencies from 'Components/Currencies/Currencies';

const mapStateToProps = (state) => ({
  user: state.user,
});

export default connect(
  mapStateToProps,
  null
)(Currencies);
