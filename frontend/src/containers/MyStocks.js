import { connect } from 'react-redux';
import MyStocks from 'Components/MyStocks/MyStocks';

const mapStateToProps = (state) => ({
  user: state.user,
});

export default connect(
  mapStateToProps,
  null
)(MyStocks);
