import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { updateConfig } from 'Redux/actions/actions';
import Config from 'Components/Config/Config';

const mapStateToProps = (state) => ({
  user: state.user,
});

const mapDispatchToProps = (dispatch) => ({
  onUpdateConfig: (payload) => {
    dispatch(updateConfig(payload));
  },
});

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(Config)
);
