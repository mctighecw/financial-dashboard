import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { setUserInfo } from 'Redux/actions/actions';
import Login from 'Components/Auth/Login/Login';

const mapStateToProps = (state) => ({
  user: state.user,
});

const mapDispatchToProps = (dispatch) => ({
  onSetUserInfo: (info, config, stocks) => {
    dispatch(setUserInfo(info, config, stocks));
  },
});

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(Login)
);
