import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { updateConfig, updateStocks } from 'Redux/actions/actions';
import EditGraphs from 'Components/EditGraphs/EditGraphs';

const mapStateToProps = (state) => ({
  user: state.user,
});

const mapDispatchToProps = (dispatch) => ({
  onUpdateConfig: (payload) => {
    dispatch(updateConfig(payload));
  },
  onUpdateStocks: (payload) => {
    dispatch(updateStocks(payload));
  },
});

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(EditGraphs)
);
