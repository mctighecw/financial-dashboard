import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { saveStockData, saveIndicatorData } from 'Redux/actions/actions';
import RenderIndicators from 'Components/RenderFinancials/Indicators/Indicators';

const mapStateToProps = (state) => ({
  user: state.user,
  stocks: state.stocks,
  indicators: state.indicators,
});

const mapDispatchToProps = (dispatch) => ({
  onSaveStockData: (payload) => {
    dispatch(saveStockData(payload));
  },
  onSaveIndicatorData: (payload) => {
    dispatch(saveIndicatorData(payload));
  },
});

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(RenderIndicators)
);
