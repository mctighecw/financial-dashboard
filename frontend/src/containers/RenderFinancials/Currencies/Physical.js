import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { saveCurrencyData } from 'Redux/actions/actions';
import RenderPhysicalCurrencies from 'Components/RenderFinancials/Currencies/Physical';

const mapStateToProps = (state) => ({
  user: state.user,
  currencies: state.currencies,
});

const mapDispatchToProps = (dispatch) => ({
  onSaveCurrencyData: (payload) => {
    dispatch(saveCurrencyData(payload));
  },
});

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(RenderPhysicalCurrencies)
);
