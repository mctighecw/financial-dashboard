import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { saveStockData } from 'Redux/actions/actions';
import RenderTrends from 'Components/RenderFinancials/Trends/Trends';

const mapStateToProps = (state) => ({
  user: state.user,
  stocks: state.stocks,
});

const mapDispatchToProps = (dispatch) => ({
  onSaveStockData: (payload) => {
    dispatch(saveStockData(payload));
  },
});

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(RenderTrends)
);
