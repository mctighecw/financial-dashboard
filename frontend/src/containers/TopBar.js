import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { clearUserInfo, clearStockData, clearCurrencyData, clearIndicatorData } from 'Redux/actions/actions';
import TopBar from 'Components/TopBar/TopBar';

const mapStateToProps = (state) => ({
  user: state.user,
});

const mapDispatchToProps = (dispatch) => ({
  onClearAllInfo: () => {
    dispatch(clearUserInfo());
    dispatch(clearStockData());
    dispatch(clearCurrencyData());
    dispatch(clearIndicatorData());
  },
});

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(TopBar)
);
