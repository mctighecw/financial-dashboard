import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { updateCurrencies } from 'Redux/actions/actions';
import PhysicalCurrenciesEditor from 'Components/Editors/Currencies/Physical';

const mapStateToProps = (state) => ({
  currencies: state.user.financials.currencies,
});

const mapDispatchToProps = (dispatch) => ({
  onUpdateCurrencies: (payload) => {
    dispatch(updateCurrencies(payload));
  },
});

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(PhysicalCurrenciesEditor)
);
