import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { updateCryptoCurrencies } from 'Redux/actions/actions';
import CryptoCurrenciesEditor from 'Components/Editors/Currencies/Crypto';

const mapStateToProps = (state) => ({
  crypto_currencies: state.user.financials.crypto_currencies,
});

const mapDispatchToProps = (dispatch) => ({
  onUpdateCryptoCurrencies: (payload) => {
    dispatch(updateCryptoCurrencies(payload));
  },
});

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(CryptoCurrenciesEditor)
);
