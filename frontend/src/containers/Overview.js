import { connect } from 'react-redux';
import Overview from 'Components/Overview/Overview';

const mapStateToProps = (state) => ({
  user: state.user,
});

export default connect(
  mapStateToProps,
  null
)(Overview);
