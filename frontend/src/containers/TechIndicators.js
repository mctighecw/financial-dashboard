import { connect } from 'react-redux';
import TechIndicators from 'Components/TechIndicators/TechIndicators';

const mapStateToProps = (state) => ({
  user: state.user,
});

export default connect(
  mapStateToProps,
  null
)(TechIndicators);
