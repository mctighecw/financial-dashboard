import { connect } from 'react-redux';
import { setUserInfo } from 'Redux/actions/actions';
import MainRoutes from 'Components/MainRoutes';

const mapStateToProps = (state) => ({
  user: state.user,
});

const mapDispatchToProps = (dispatch) => ({
  onSetUserInfo: (payload) => {
    dispatch(setUserInfo(payload));
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(MainRoutes);
