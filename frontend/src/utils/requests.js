import axios from 'axios';

const api = axios.create({
  timeout: 10000,
  headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
  withCredentials: true,
});

export const get = (url) => {
  return api.get(url);
};

export const post = (url, data) => {
  return api.post(url, data);
};
