const getPrefix = () => {
  const prefix = process.env.NODE_ENV === 'development' ? 'http://localhost:9200' : '';
  return prefix;
};

const getUrl = (category, type) => {
  const prefix = getPrefix();
  return `${prefix}/api/${category}/${type}`;
};

export const getAuthUrl = (type) => getUrl('auth', type);
export const getUserUrl = (type) => getUrl('user', type);
export const getStocksUrl = (type) => getUrl('stocks', type);
export const getCurrenciesUrl = (type) => getUrl('currencies', type);
