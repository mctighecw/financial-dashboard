import moment from 'moment';

const getOffset = (datestring) => {
  // Adjusts time according to timezone
  const offset = moment().utcOffset();
  const currentDateTime = moment
    .utc(datestring)
    .utcOffset(offset)
    .format();
  return currentDateTime;
};

export const getDateNow = () => {
  // Returns date object now
  const now = new Date();
  return now;
};

export const getDateStringNow = () => {
  // Returns date now as ISO string
  const now = getDateNow();
  const dateTime = now.toISOString();
  return dateTime;
};

export const dateMonthDayYear = (datestring) => {
  // Converts a datestring 2020-01-01 into the format January 1, 2020
  const currentDateTime = getOffset(datestring);
  const date = moment(currentDateTime).format('MMMM DD, YYYY');
  return date;
};

export const dateDayMonthYear = (datestring) => {
  // Converts a datestring 2020-01-01 into the format 1 January 2020
  const currentDateTime = getOffset(datestring);
  const date = moment(currentDateTime).format('DD MMMM YYYY');
  return date;
};

export const dateMonthDay = (datestring) => {
  // Converts a datestring 2020-01-01 into the short format January 1
  const dateTime = moment(datestring, 'YYYY-MM-DD').format('MMM DD');
  return dateTime;
};

export const dateDayMonth = (datestring) => {
  // Converts a datestring 2020-01-01 into the short format 1 Jan
  const dateTime = moment(datestring, 'YYYY-MM-DD').format('DD MMM');
  return dateTime;
};

export const time24Hour = (datestring) => {
  // Converts a long date/time string "2020-01-01 14:30:20"
  // into the time format 14:30
  const split1 = datestring.split(' ');
  const time = split1[1];
  const split2 = time.split(/:/g);
  const hours = split2[0];
  const minutes = split2[1];
  const hoursMinutes = `${hours}:${minutes}`;
  return hoursMinutes;
};

export const timeAMPM = (datestring) => {
  // Converts a 24 hour time 14:30 into the format 02:30 pm
  const time = moment(datestring, 'HH:mm').format('hh:mm a');
  return time;
};
