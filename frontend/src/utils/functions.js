import moment from 'moment';

export const getErrorMessage = (err) => {
  let result = 'An error has occurred';

  if (err.response && err.response.data && err.response.data.message) {
    result = err.response.data.message;
  }
  return result;
};

export const redirectToLogin = (err, history, callBack = () => {}) => {
  if (err.response && err.response.status && err.response.status === 401) {
    history.push('/login');
  } else {
    callBack();
  }
};

const shouldUpdateData = (timestamp) => {
  const now = moment(new Date());
  const last = moment(timestamp);
  const duration = moment.duration(now.diff(last));
  const minutes = duration.asMinutes();
  let update = false;

  if (minutes > 5.0) {
    const min = minutes.toFixed();
    console.log(`${min} min. since last update`);
    update = true;
  }
  return update;
};

export const handleCheckUpdate = (data, key) => {
  let result = true;

  if (data[key] && data[key].timestamp) {
    const { timestamp } = data[key];
    result = shouldUpdateData(timestamp);
  }
  return result;
};

export const handleCheckIndicatorUpdate = (data, key, type) => {
  let result = true;

  if (data[key] && data[key][type] && data[key][type].timestamp) {
    const { timestamp } = data[key][type];
    result = shouldUpdateData(timestamp);
  }
  return result;
};

export const makeCurrencyKey = (item) => {
  const key = item.from_symbol + '_' + item.to_symbol;
  return key;
};

export const makeCryptoKey = (item) => {
  const key = item.symbol + '_' + item.market;
  return key;
};

export const getGraphSize = (user) => {
  const graphSize = user.config.graph_size.replace(' ', '-');
  return graphSize;
};

export const formatStockSymbol = (value) => {
  // Only allow letters and a single, non-consecutive . or -
  let result = value.replace(/[^a-zA-Z.-]+/g, '');
  result = result.replace('..', '.');
  result = result.replace('.-', '.');
  result = result.replace('--', '-');
  result = result.replace('-.', '-');
  result = result.toUpperCase();
  return result;
};

const ensureFourDigits = (value) => {
  let result = value;

  if (value.length < 4) {
    result = value.padEnd(4, '0');
  }
  return result;
};

export const formatLongNumber = (number, ensureDecimal = false) => {
  const split = number.toString().split('.');
  const split0 = split[0].replace(/\B(?=(\d{3})+(?!\d))/g, ',');
  const split1 = split[1] ? ensureFourDigits(split[1]) : '';
  const joiner = split1 !== '' ? '.' : '';
  let result = split0 + joiner + split1;

  if (ensureDecimal) {
    result = split0 + '.' + ensureFourDigits(split1);
  }
  return result;
};

export const kFormatter = (num) => {
  // Shortens long numbers with a "k", e.g. 200000 becomes 200k
  return Math.abs(num) > 999
    ? Math.sign(num) * (Math.abs(num) / 1000).toFixed(1) + 'k'
    : Math.sign(num) * Math.abs(num);
};
