import { dateMonthDay, dateDayMonth, time24Hour, timeAMPM } from './timeFunctions';
import colorSchemes from 'Styles/colorSchemes';
import { WHITE, GREY } from 'Styles/colors';

export const parseDate = (data, dateFormat) => {
  let labels = [];

  Object.keys(data).forEach((key) => {
    const date = dateFormat === 'month-day' ? dateMonthDay(key) : dateDayMonth(key);
    labels.push(date);
  });
  return labels;
};

export const parseTime = (data, timeFormat) => {
  let labels = [];

  Object.keys(data).forEach((key) => {
    const hoursMinutes = time24Hour(key);
    const res = timeFormat === 'am-pm' ? timeAMPM(hoursMinutes) : hoursMinutes;
    labels.push(res);
  });
  return labels;
};

export const parseCurrencyData = (data) => {
  const parsedData = {};
  const line = Object.values(data)[0];
  const keys = Object.keys(line);

  keys.forEach((key) => {
    parsedData[key] = [];
  });

  Object.values(data).forEach((value) => {
    keys.forEach((key) => {
      parsedData[key].push(value[key]);
    });
  });

  return parsedData;
};

export const parseGraphData = (data, graphType) => {
  const parsedData = {};
  const line = Object.values(data)[0][graphType];
  const keys = Object.keys(line);

  keys.forEach((key) => {
    parsedData[key] = [];
  });

  Object.values(data).forEach((value) => {
    keys.forEach((key) => {
      parsedData[key].push(value[graphType][key]);
    });
  });

  return parsedData;
};

export const parseTrendData = (data) => {
  const parsedData = [];

  const keys = {
    open: 'o',
    high: 'h',
    low: 'l',
    close: 'c',
  };

  Object.values(data).forEach((value) => {
    const newLine = {};

    Object.keys(value.line).forEach((key) => {
      const newKey = keys[key];
      newLine[newKey] = value.line[key];
    });
    parsedData.push(newLine);
  });

  return parsedData;
};

export const parseIndicatorData = (data, date) => {
  const parsedData = {};
  const line = Object.values(data)[0];
  const keys = Object.keys(line);

  keys.forEach((key) => {
    parsedData[key] = [];
  });

  Object.keys(data).forEach((timeKey) => {
    if (timeKey.includes(date)) {
      keys.forEach((key) => {
        parsedData[key].push(data[timeKey][key]);
      });
    }
  });

  return parsedData;
};

export const ensureMinLength = (data) => {
  const minLength = 28;
  let result = data;

  if (result.length < minLength) {
    const diffArray = [];
    const diff = minLength - result.length;

    [...Array(diff).keys()].forEach((item) => {
      diffArray.push('');
    });
    result = [...result, ...diffArray];
  }
  return result;
};

export const renderDataSets = (parsedData, scheme) => {
  const dataSets = [];
  const colorPalette = colorSchemes[scheme - 1];

  const baseConfig = (index) => ({
    fill: false,
    lineTension: 0.1,
    backgroundColor: colorPalette[index],
    borderColor: colorPalette[index],
    borderCapStyle: 'butt',
    borderDash: [],
    borderDashOffset: 0.0,
    borderJoinStyle: 'miter',
    borderWidth: 3,
    pointBorderColor: colorPalette[index],
    pointBackgroundColor: WHITE,
    pointBorderWidth: 1,
    pointHoverRadius: 5,
    pointHoverBackgroundColor: colorPalette[index],
    pointHoverBorderColor: GREY,
    pointHoverBorderWidth: 2,
    pointRadius: 1,
    pointHitRadius: 10,
  });

  Object.keys(parsedData).forEach((key, index) => {
    dataSets.push({
      ...baseConfig(index),
      label: key,
      data: parsedData[key],
    });
  });
  return dataSets;
};
