import React from 'react';
import RenderStocks from 'Containers/RenderFinancials/Stocks';
import './styles.scss';

const MyStocks = (props) => {
  const { stocks } = props.user.financials;

  return (
    <div styleName="container">
      <div styleName="heading">My Stocks</div>
      <RenderStocks symbols={stocks} />
    </div>
  );
};

export default MyStocks;
