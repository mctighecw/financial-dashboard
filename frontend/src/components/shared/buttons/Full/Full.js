import React from 'react';
import './styles.scss';

const Full = ({ label, disabled, onClickMethod }) => (
  <div styleName="container">
    <button styleName="full-button" disabled={disabled} onClick={onClickMethod}>
      {label}
    </button>
  </div>
);

export default Full;
