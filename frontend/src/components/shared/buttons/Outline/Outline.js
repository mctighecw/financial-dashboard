import React from 'react';
import './styles.scss';

const Outline = ({ label, disabled, onClickMethod }) => (
  <div styleName="container">
    <button styleName="outline-button" disabled={disabled} onClick={onClickMethod}>
      {label}
    </button>
  </div>
);

export default Outline;
