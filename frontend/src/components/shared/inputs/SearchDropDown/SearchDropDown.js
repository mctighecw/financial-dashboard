import React from 'react';
import { post } from 'Utils/requests';
import { withRouter } from 'react-router-dom';
import { getStocksUrl } from 'Utils/network';
import { formatStockSymbol, redirectToLogin } from 'Utils/functions';

import CloseIcon from 'Assets/close.svg';
import './styles.scss';

class SearchDropDown extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      showOptions: false,
      options: [],
    };
  }

  handleEnterValue = (event) => {
    const value = formatStockSymbol(event.target.value);

    this.props.setStock(value);

    if (value !== '' && value.length > 1) {
      this.searchStockSymbols(value);
    }
  };

  searchStockSymbols = (term) => {
    const { history } = this.props;
    const url = getStocksUrl('symbol_search');
    const data = JSON.stringify({ search_term: term });

    post(url, data)
      .then((res) => {
        const searchData = res.data.search_data;
        this.setState({ options: searchData, showOptions: true });
      })
      .catch((err) => {
        console.log(err);
        redirectToLogin(err, history);
      });
  };

  handleSelectStock = (value) => {
    this.setState({ showOptions: false });
    this.props.setStock(value);
  };

  render() {
    const { showOptions, options } = this.state;
    const { value, maxLength, placeholder } = this.props;

    return (
      <div styleName="container">
        <div styleName="content" style={{ zIndex: showOptions ? 10 : 1 }}>
          {value !== '' && (
            <div styleName="close" onClick={() => this.handleSelectStock('')}>
              <img src={CloseIcon} alt="" />
            </div>
          )}
          <input
            type="text"
            value={value}
            maxLength={maxLength}
            placeholder={placeholder}
            onChange={this.handleEnterValue}
          />

          {showOptions &&
            options.length > 0 &&
            options.map((item, index) => {
              const style = index === 0 ? 'choice first' : 'choice';

              return (
                <div
                  key={index}
                  value={item.symbol}
                  styleName={style}
                  onClick={() => this.handleSelectStock(item.symbol)}
                >
                  {`${item.symbol} — ${item.name}`}
                </div>
              );
            })}
        </div>
      </div>
    );
  }
}

export default withRouter(SearchDropDown);
