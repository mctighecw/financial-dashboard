import React from 'react';
import './styles.scss';

const AuthInput = ({ type, value, name, maxLength, placeholder, onKeyPressMethod, onChangeMethod }) => (
  <div styleName="container">
    <input
      type={type}
      value={value}
      name={name}
      maxLength={maxLength}
      autofill="false"
      placeholder={placeholder}
      onKeyPress={onKeyPressMethod}
      onChange={onChangeMethod}
    />
  </div>
);

export default AuthInput;
