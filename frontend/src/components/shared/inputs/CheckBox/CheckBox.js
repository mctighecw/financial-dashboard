import React from 'react';
import './styles.scss';

const CheckBox = (props) => {
  const { label, checked, onChangeMethod } = props;

  return (
    <div styleName="container">
      <div styleName="content">
        <label styleName="label">
          {label}
          <input type="checkbox" checked={checked} onChange={onChangeMethod} />
          <span styleName="checkmark" />
        </label>
      </div>
    </div>
  );
};

export default CheckBox;
