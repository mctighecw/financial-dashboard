import React from 'react';
import { Link } from 'react-router-dom';
import links from './links';
import './styles.scss';

const SidePanel = () => {
  const renderLinks = () => {
    const divs = [];

    links.map((item, index) =>
      divs.push(
        <Link key={index} to={item.path}>
          <div styleName="menu-item">
            <img src={item.icon} alt={item.name} />
            <div styleName="text">{item.name}</div>
          </div>
        </Link>
      )
    );
    return divs;
  };

  return <div styleName="container">{renderLinks()}</div>;
};

export default SidePanel;
