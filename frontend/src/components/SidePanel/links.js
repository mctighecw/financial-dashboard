import DashboardIcon from 'Assets/dashboard.svg';
import GraphIcon1 from 'Assets/graph1.svg';
import GraphIcon2 from 'Assets/graph2.svg';
import CurrencyIcon from 'Assets/currency.svg';
import AnalyticsIcon from 'Assets/analysis.svg';
import AddIcon from 'Assets/add.svg';
import ConfigIcon from 'Assets/configuration.svg';
import InfoIcon from 'Assets/info.svg';

const links = [
  {
    name: 'Overview',
    path: '/overview',
    icon: DashboardIcon,
  },
  {
    name: 'My Stocks',
    path: '/my-stocks',
    icon: GraphIcon1,
  },
  {
    name: 'Tech Indicators',
    path: '/tech-indicators',
    icon: GraphIcon2,
  },
  {
    name: 'Currencies',
    path: '/currencies',
    icon: CurrencyIcon,
  },
  // {
  //   name: 'Analytics',
  //   path: '/analytics',
  //   icon: AnalyticsIcon,
  // },
  {
    name: 'Edit Graphs',
    path: '/edit-graphs',
    icon: AddIcon,
  },
  {
    name: 'Config',
    path: '/config',
    icon: ConfigIcon,
  },
  {
    name: 'Info',
    path: '/info',
    icon: InfoIcon,
  },
];

export default links;
