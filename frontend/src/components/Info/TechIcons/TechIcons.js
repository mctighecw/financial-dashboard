import React from 'react';
import icons from './icons';
import './styles.scss';

const TechIcons = () => {
  const renderIcons = () => {
    return icons.map((item, index) => (
      <img key={index} src={item.icon} alt={item.label} title={item.label} />
    ));
  };

  return <div styleName="container">{renderIcons()}</div>;
};

export default TechIcons;
