import ReactLogo from 'Assets/tech/react.svg';
import ReduxLogo from 'Assets/tech/redux.svg';
import PythonLogo from 'Assets/tech/python.svg';
import MongoDBLogo from 'Assets/tech/mongodb.svg';

export const icons = [
  { label: 'React', icon: ReactLogo },
  { label: 'Redux', icon: ReduxLogo },
  { label: 'Python Flask', icon: PythonLogo },
  { label: 'MongoDB', icon: MongoDBLogo },
];

export default icons;
