import React from 'react';
import './styles.scss';

const Footer = () => (
  <div styleName="container">
    <div styleName="copyright">&copy; 2019 Christian McTighe. Coded by Hand.</div>
  </div>
);

export default Footer;
