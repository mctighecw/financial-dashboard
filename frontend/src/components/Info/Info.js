import React from 'react';
import TechIcons from './TechIcons/TechIcons';
import Footer from './Footer/Footer';

import links from './links';
import './styles.scss';

const Info = () => {
  const renderLink = (name) => {
    const link = links[name];

    return (
      <a href={link.url} target="_blank">
        {link.label}
      </a>
    );
  };

  return (
    <div styleName="container">
      <div styleName="content">
        <div styleName="heading">Info</div>

        <div styleName="box">
          <div styleName="sub-heading">Overview</div>
          <div styleName="paragraph">
            The Financial Dashboard provides the latest stock market info, as well as physical currency and
            cryptocurrency rates.
          </div>

          <div styleName="paragraph">
            Users can sign up, log in, then choose which stocks and currencies to follow. Each time users log
            in, they get either live or the most recent financial data displayed on graphs. There are also
            some graphs that display technical indicators, such as the simple moving average and the relative
            strength index. Finally, various options (e.g. grid lines, date & time formats, color schemes) can
            be customized.
          </div>

          <div styleName="paragraph">
            <span>DISCLAIMER:</span> The content on this site is provided solely for informational purposes.
            It is not intended to be used to make real financial or investment decisions. The site's creator
            assumes no liability for any financial loss resulting from use of the charts, data or any other
            information provided.
          </div>

          <div styleName="sub-heading extra-margin-top">Tech Stack</div>

          <div styleName="paragraph">
            This app was built with modern web technologies: {renderLink('react')}, {renderLink('redux')},{' '}
            {renderLink('flask')}, and {renderLink('mongodb')}.
          </div>

          <TechIcons />
        </div>
      </div>

      <Footer />
    </div>
  );
};

export default Info;
