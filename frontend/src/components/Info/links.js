const links = {
  react: { label: 'React', url: 'https://reactjs.org' },
  redux: { label: 'Redux', url: 'https://redux.js.org' },
  flask: { label: 'Python Flask', url: 'https://palletsprojects.com/p/flask' },
  mongodb: { label: 'MongoDB', url: 'https://www.mongodb.com' },
};

export default links;
