import React from 'react';
import { post } from 'Utils/requests';
import { getUserUrl } from 'Utils/network';
import { redirectToLogin } from 'Utils/functions';

import StocksEditor from 'Components/Editors/Stocks/Stocks';
import PhysicalCurrenciesEditor from 'Containers/Editors/Currencies/Physical';
import CryptoCurrenciesEditor from 'Containers/Editors/Currencies/Crypto';

import './styles.scss';

const EditGraphs = (props) => {
  const { history, user, onUpdateConfig, onUpdateStocks } = props;

  const handleUpdateConfig = (field, value) => {
    const url = getUserUrl('config_update');
    const data = JSON.stringify({ field, value });

    post(url, data)
      .then((res) => {
        const { data } = res;
        onUpdateConfig(data.config);
      })
      .catch((err) => {
        console.log(err);
        redirectToLogin(err, history);
      });
  };

  const handleUpdateStocks = (values) => {
    const url = getUserUrl('stocks_update');
    const data = JSON.stringify({ values });

    post(url, data)
      .then((res) => {
        const { data } = res;
        onUpdateStocks(data.stocks);
      })
      .catch((err) => {
        console.log(err);
        redirectToLogin(err, history);
      });
  };

  return (
    <div styleName="container">
      <div styleName="heading margin-bottom">Edit Graphs</div>

      <div styleName="settings margin-bottom">
        <div styleName="heading">Overview page</div>
        <div styleName="sub-heading">Add up to four stocks to show on the overview page</div>

        <StocksEditor
          title="Overview graphs"
          limit={4}
          stocks={user.config.overview_graphs}
          handleUpdate={(value) => handleUpdateConfig('overview_graphs', value)}
        />
      </div>

      <div styleName="settings margin-bottom">
        <div styleName="heading">My stocks</div>
        <div styleName="sub-heading">Add some stocks to follow</div>

        <StocksEditor
          title="My stocks"
          limit={10}
          stocks={user.financials.stocks}
          handleUpdate={handleUpdateStocks}
        />
      </div>

      <div styleName="settings margin-bottom">
        <div styleName="heading">Physical currencies</div>
        <div styleName="sub-heading">Add a new pair of currency exchange rates to follow</div>

        <PhysicalCurrenciesEditor limit={4} />
      </div>

      <div styleName="settings margin-bottom">
        <div styleName="heading">Cryptocurrencies</div>
        <div styleName="sub-heading">Add a new cryptocurrency and physical currency pair to follow</div>

        <CryptoCurrenciesEditor limit={4} />
      </div>
    </div>
  );
};

export default EditGraphs;
