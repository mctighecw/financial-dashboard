import React, { useState } from 'react';
import infoIcon from 'Assets/info.svg';
import './styles.scss';

const MetaDataBox = (props) => {
  const [showInfo, useShowInfo] = useState(false);

  const renderFields = (data) => {
    const divs = [];

    Object.keys(data).map((item, index) => {
      const heading = item.split(/ (.+)/)[1];
      const info = data[item];

      divs.push(
        <div key={index} styleName="line">
          <span>{`${heading}:`}</span>
          {info}
        </div>
      );
    });
    return divs;
  };

  return (
    <div styleName="container">
      <img
        src={infoIcon}
        alt="Info"
        onMouseOver={(event) => useShowInfo(true)}
        onMouseLeave={(event) => useShowInfo(false)}
      />

      {showInfo && <div styleName="box">{renderFields(props.data)}</div>}
    </div>
  );
};

export default MetaDataBox;
