import React from 'react';
import { isMobile } from 'react-device-detect';

import MobileMessage from 'Components/MobileMessage/MobileMessage';
import App from 'Components/App';

const Root = () => <div>{isMobile && window.innerWidth < 700 ? <MobileMessage /> : <App />}</div>;

export default Root;
