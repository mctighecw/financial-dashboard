import React from 'react';
import OutlineButton from 'Components/shared/buttons/Outline/Outline';
import './styles.scss';

const ReloadButton = ({ onClickMethod }) => (
  <div styleName="container">
    <OutlineButton label="Try again" onClickMethod={onClickMethod} />
  </div>
);

export default ReloadButton;
