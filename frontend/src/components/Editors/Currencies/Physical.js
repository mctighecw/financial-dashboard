import React, { useState } from 'react';
import { post } from 'Utils/requests';
import { getUserUrl } from 'Utils/network';
import { redirectToLogin } from 'Utils/functions';

import DropDown from 'Components/shared/inputs/DropDown/DropDown';
import OutlineButton from 'Components/shared/buttons/Outline/Outline';
import currencyOptions from 'Components/Config/options/currencies';

import TrashIcon from 'Assets/trash.svg';
import './styles.scss';

const PhysicalCurrenciesEditor = (props) => {
  const { history, limit, currencies, onUpdateCurrencies } = props;
  const [newCurrencyFrom, useNewCurrencyFrom] = useState('');
  const [newCurrencyTo, useNewCurrencyTo] = useState('');

  const renderCurrencies = (currencies) => {
    const divs = [];

    Object.keys(currencies).length > 0
      ? currencies.map((item, index) => {
          const name = item.from_symbol + ' - ' + item.to_symbol;

          divs.push(
            <div key={index} styleName="currency">
              <div styleName="label">{name}</div>
              <img src={TrashIcon} alt="Delete" onClick={() => deleteCurrency(item)} />
            </div>
          );
        })
      : divs.push(
          <div key="none" styleName="currency">
            <div styleName="none">NONE</div>
          </div>
        );
    return divs;
  };

  const deleteCurrency = (item) => {
    const { from_symbol, to_symbol } = item;
    const values = { type: 'delete', from_symbol, to_symbol };
    updateCurrencies(values);
  };

  const addNewCurrency = () => {
    const values = { type: 'add', from_symbol: newCurrencyFrom, to_symbol: newCurrencyTo };
    updateCurrencies(values);
    useNewCurrencyFrom('');
    useNewCurrencyTo('');
  };

  const updateCurrencies = (values) => {
    const url = getUserUrl('currencies_update');
    const data = JSON.stringify(values);

    post(url, data)
      .then((res) => {
        const { data } = res;
        onUpdateCurrencies(data.currencies);
      })
      .catch((err) => {
        console.log(err);
        redirectToLogin(err, history);
      });
  };

  const buttonDisabled =
    newCurrencyFrom === '' ||
    newCurrencyTo === '' ||
    newCurrencyFrom === newCurrencyTo ||
    Object.keys(currencies).length === limit;

  return (
    <div styleName="container">
      <div styleName="my-currencies">
        <div styleName="title">My currencies:</div>
        {renderCurrencies(currencies)}
      </div>

      <div styleName="add-currencies">
        <div>
          <DropDown
            size="normal"
            placeholder="From currency"
            value={newCurrencyFrom}
            options={currencyOptions}
            onChangeMethod={(event) => useNewCurrencyFrom(event.target.value)}
          />
        </div>

        <div>
          <DropDown
            size="normal"
            placeholder="To currency"
            value={newCurrencyTo}
            options={currencyOptions}
            onChangeMethod={(event) => useNewCurrencyTo(event.target.value)}
          />
        </div>

        <OutlineButton label="Add" disabled={buttonDisabled} onClickMethod={() => addNewCurrency()} />
      </div>
    </div>
  );
};

export default PhysicalCurrenciesEditor;
