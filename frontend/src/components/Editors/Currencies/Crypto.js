import React, { useState } from 'react';
import { post } from 'Utils/requests';
import { getUserUrl } from 'Utils/network';
import { redirectToLogin } from 'Utils/functions';

import DropDown from 'Components/shared/inputs/DropDown/DropDown';
import OutlineButton from 'Components/shared/buttons/Outline/Outline';
import cryptoCurrencyOptions from 'Components/Config/options/cryptoCurrencies';
import currencyOptions from 'Components/Config/options/currencies';

import TrashIcon from 'Assets/trash.svg';
import './styles.scss';

const CryptoCurrenciesEditor = (props) => {
  const { history, limit, crypto_currencies, onUpdateCryptoCurrencies } = props;
  const [newSymbol, useNewSymbol] = useState('');
  const [newMarket, useNewMarket] = useState('');

  const renderCryptoCurrencies = (currencies) => {
    const divs = [];

    Object.keys(currencies).length > 0
      ? currencies.map((item, index) => {
          const name = item.symbol + ' - ' + item.market;

          divs.push(
            <div key={index} styleName="currency">
              <div styleName="label">{name}</div>
              <img src={TrashIcon} alt="Delete" onClick={() => deleteCryptoCurrency(item)} />
            </div>
          );
        })
      : divs.push(
          <div key="none" styleName="currency">
            <div styleName="none">NONE</div>
          </div>
        );
    return divs;
  };

  const deleteCryptoCurrency = (item) => {
    const { symbol, market } = item;
    const values = { type: 'delete', symbol, market };
    updateCryptoCurrencies(values);
  };

  const addNewCryptoCurrency = () => {
    const values = { type: 'add', symbol: newSymbol, market: newMarket };
    updateCryptoCurrencies(values);
    useNewSymbol('');
    useNewMarket('');
  };

  const updateCryptoCurrencies = (values) => {
    const url = getUserUrl('crypto_currencies_update');
    const data = JSON.stringify(values);

    post(url, data)
      .then((res) => {
        const { data } = res;
        onUpdateCryptoCurrencies(data.crypto_currencies);
      })
      .catch((err) => {
        console.log(err);
        redirectToLogin(err, history);
      });
  };

  const buttonDisabled =
    newSymbol === '' || newMarket === '' || Object.keys(crypto_currencies).length === limit;

  return (
    <div styleName="container">
      <div styleName="my-currencies">
        <div styleName="title">My cryptocurrencies:</div>
        {renderCryptoCurrencies(crypto_currencies)}
      </div>

      <div styleName="add-currencies">
        <div>
          <DropDown
            size="normal"
            placeholder="Cryptocurrency"
            value={newSymbol}
            options={cryptoCurrencyOptions}
            onChangeMethod={(event) => useNewSymbol(event.target.value)}
          />
        </div>

        <div>
          <DropDown
            size="normal"
            placeholder="Market currency"
            value={newMarket}
            options={currencyOptions}
            onChangeMethod={(event) => useNewMarket(event.target.value)}
          />
        </div>

        <OutlineButton label="Add" disabled={buttonDisabled} onClickMethod={() => addNewCryptoCurrency()} />
      </div>
    </div>
  );
};

export default CryptoCurrenciesEditor;
