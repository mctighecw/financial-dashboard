import React, { useState } from 'react';

import SearchDropDown from 'Components/shared/inputs/SearchDropDown/SearchDropDown';
import OutlineButton from 'Components/shared/buttons/Outline/Outline';

import TrashIcon from 'Assets/trash.svg';
import './styles.scss';

const StocksEditor = (props) => {
  const { title, limit, stocks, handleUpdate } = props;
  const [newStock, useNewStock] = useState('');

  const renderStocks = (stocks) => {
    const divs = [];

    stocks.length > 0
      ? stocks.map((item, index) => {
          divs.push(
            <div key={index} styleName="stock">
              <div styleName="label">{item}</div>
              <img src={TrashIcon} alt="Delete" onClick={() => deleteStock(index)} />
            </div>
          );
        })
      : divs.push(
          <div key="none" styleName="stock">
            <div styleName="none">NONE</div>
          </div>
        );
    return divs;
  };

  const deleteStock = (index) => {
    const values = [...stocks];
    values.splice(index, 1);
    handleUpdate(values);
  };

  const setStock = (value) => {
    useNewStock(value);
  };

  const addNewStock = () => {
    const values = [...stocks, newStock];
    handleUpdate(values);
    useNewStock('');
  };

  const buttonDisabled = newStock === '' || (limit && stocks.length === limit);

  return (
    <div styleName="container">
      <div styleName="my-stocks">
        <div styleName="title">{`${title}:`}</div>
        {renderStocks(stocks)}
      </div>

      <div styleName="add-stock">
        <SearchDropDown
          value={newStock}
          maxLength={5}
          placeholder={stocks.length === 0 ? 'Add a stock' : 'Add another'}
          setStock={setStock}
        />

        <OutlineButton label="Add" disabled={buttonDisabled} onClickMethod={addNewStock} />
      </div>
    </div>
  );
};

export default StocksEditor;
