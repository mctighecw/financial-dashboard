import React from 'react';
import ErrorIcon from 'Assets/error.svg';
import './styles.scss';

const ErrorMessage = () => (
  <div styleName="container">
    <img src={ErrorIcon} alt="!" />
    <div styleName="message">Error loading data (there is a maximum number of requests per minute)</div>
  </div>
);

export default ErrorMessage;
