import React, { useEffect } from 'react';
import { Switch, Route, Redirect, useHistory, useLocation } from 'react-router-dom';
import { get } from 'Utils/requests';
import { getAuthUrl } from 'Utils/network';

import Login from 'Containers/Login';
import SignUp from 'Components/Auth/SignUp/SignUp';
import Main from 'Components/Main/Main';

import '../styles/global.css';

const MainRoutes = ({ user, onSetUserInfo }) => {
  const history = useHistory();
  const location = useLocation();

  const checkUserAuth = () => {
    const url = getAuthUrl('check');

    get(url)
      .then((res) => {
        const { status, info, config, financials } = res.data;

        if (status === 'OK') {
          onSetUserInfo({ info, config, financials });

          history.replace({
            pathname: location?.pathname || '/overview',
            state: { from: location },
          });
        }
      })
      .catch((err) => {
        console.log(err);
      });
  };

  useEffect(() => {
    if (!user?.info?.username) {
      checkUserAuth();
    }
  }, [user]);

  if (user?.info?.username) {
    return (
      <Switch>
        <Route component={Main} />
      </Switch>
    );
  }

  return (
    <Switch>
      <Route path="/login" component={Login} />
      <Route path="/sign-up" component={SignUp} />
      <Route render={() => <Redirect to="/login" />} />
    </Switch>
  );
};

export default MainRoutes;
