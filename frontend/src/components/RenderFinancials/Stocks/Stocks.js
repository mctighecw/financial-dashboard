import React from 'react';
import { post } from 'Utils/requests';
import { dateMonthDayYear, dateDayMonthYear } from 'Utils/timeFunctions';
import { getGraphSize, handleCheckUpdate, redirectToLogin } from 'Utils/functions';
import { getStocksUrl } from 'Utils/network';

import Loading from 'Components/Loading/Loading';
import MetaDataBox from 'Components/MetaDataBox/MetaDataBox';
import LineGraph from 'Components/GraphTypes/LineGraph/LineGraph';
import BarGraph from 'Components/GraphTypes/BarGraph/BarGraph';
import ErrorMessage from 'Components/ErrorMessage/ErrorMessage';
import ReloadButton from 'Components/ReloadButton/ReloadButton';

import '../styles.scss';

class RenderStocks extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      error: false,
    };
  }

  handleLoadData = () => {
    const { stocks, symbols } = this.props;

    if (symbols.length > 0) {
      symbols.forEach((symbol) => {
        const shouldUpdateData = handleCheckUpdate(stocks, symbol);
        if (shouldUpdateData) this.handleStocksRequest(symbol);
      });
    }
  };

  handleStocksRequest = (symbol) => {
    const url = getStocksUrl('time_series');
    const data = JSON.stringify({ symbol });

    post(url, data)
      .then((res) => {
        const { meta_data, date, data } = res.data;

        this.props.onSaveStockData({
          symbol,
          date,
          data,
          metaData: meta_data,
        });
      })
      .catch((err) => {
        console.log(err);

        const setError = () => {
          this.setState({ error: true });
        };
        redirectToLogin(err, this.props.history, setError);
      });
  };

  renderStocks = () => {
    const { symbols, stocks, user } = this.props;
    const graphSize = getGraphSize(user);
    const { show_volume_graph, show_graph_lines, graph_color_scheme, time_format, date_format } = user.config;
    const divs = [];

    symbols.map((symbol, index) => {
      const date =
        date_format === 'month-day'
          ? dateMonthDayYear(stocks[symbol].date)
          : dateDayMonthYear(stocks[symbol].date);

      divs.push(
        <div key={index} styleName={`graph ${graphSize}`}>
          <div styleName="top-row">
            <div styleName="title">{symbol}</div>
            <MetaDataBox data={stocks[symbol].metaData} />
          </div>
          <LineGraph
            type="stock"
            allData={stocks[symbol]}
            showLines={show_graph_lines}
            colorScheme={graph_color_scheme}
            timeFormat={time_format}
            dateFormat={date_format}
          />
          {show_volume_graph && <BarGraph stocksData={stocks[symbol]} timeFormat={time_format} />}
          <div styleName="date">{date}</div>
        </div>
      );
    });

    if (divs.length > 0) {
      return divs;
    } else {
      return <div styleName="none">None yet</div>;
    }
  };

  handleReloadData = () => {
    this.setState({ error: false }, () => {
      this.handleLoadData();
    });
  };

  componentDidMount() {
    this.handleLoadData();
  }

  componentDidUpdate(prevProps) {
    if (prevProps.symbols.length !== this.props.symbols.length) {
      this.handleLoadData();
    }
  }

  render() {
    const { error } = this.state;
    const { symbols, stocks } = this.props;
    let stockSymbols = 0;

    symbols.forEach((symbol) => {
      if (Object.keys(stocks).includes(symbol)) {
        stockSymbols += 1;
      }
    });

    return (
      <div>
        {stockSymbols !== symbols.length && !error && <Loading />}
        {stockSymbols === symbols.length && !error && <div styleName="container">{this.renderStocks()}</div>}
        {error && (
          <div>
            <ErrorMessage />
            <ReloadButton onClickMethod={this.handleReloadData} />
          </div>
        )}
      </div>
    );
  }
}

export default RenderStocks;
