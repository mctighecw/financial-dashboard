import React from 'react';
import { post } from 'Utils/requests';
import { dateMonthDayYear, dateDayMonthYear } from 'Utils/timeFunctions';
import { getGraphSize, handleCheckUpdate, redirectToLogin } from 'Utils/functions';
import { getStocksUrl } from 'Utils/network';

import Loading from 'Components/Loading/Loading';
import MetaDataBox from 'Components/MetaDataBox/MetaDataBox';
import CandleStickGraph from 'Components/GraphTypes/CandleStickGraph/CandleStickGraph';
import BarGraph from 'Components/GraphTypes/BarGraph/BarGraph';
import ErrorMessage from 'Components/ErrorMessage/ErrorMessage';
import ReloadButton from 'Components/ReloadButton/ReloadButton';

import '../styles.scss';

class RenderTrends extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      error: false,
    };
  }

  handleLoadData = () => {
    const { stocks, symbol } = this.props;

    if (symbol !== '') {
      const shouldUpdateData = handleCheckUpdate(stocks, symbol);
      if (shouldUpdateData) this.handleStocksRequest(symbol);
    }
  };

  handleStocksRequest = (symbol) => {
    const url = getStocksUrl('time_series');
    const data = JSON.stringify({ symbol });

    post(url, data)
      .then((res) => {
        const { meta_data, date, data } = res.data;

        this.props.onSaveStockData({
          symbol,
          date,
          data,
          metaData: meta_data,
        });
      })
      .catch((err) => {
        console.log(err);

        const setError = () => {
          this.setState({ error: true });
        };
        redirectToLogin(err, this.props.history, setError);
      });
  };

  renderStock = () => {
    const { symbol, stocks, user } = this.props;
    const graphSize = getGraphSize(user);
    const { show_volume_graph, show_graph_lines, time_format, date_format } = user.config;

    if (symbol !== '') {
      const date =
        date_format === 'month-day'
          ? dateMonthDayYear(stocks[symbol].date)
          : dateDayMonthYear(stocks[symbol].date);

      return (
        <div styleName={`graph ${graphSize}`}>
          <div styleName="top-row">
            <div styleName="title">{`Trends — ${symbol}`}</div>
            <MetaDataBox data={stocks[symbol].metaData} />
          </div>
          <CandleStickGraph
            stocksData={stocks[symbol]}
            showLines={show_graph_lines}
            timeFormat={time_format}
          />
          {show_volume_graph && <BarGraph stocksData={stocks[symbol]} timeFormat={time_format} />}
          <div styleName="tooltip-keys">Hover box keys: Open (O), High (H), Low (L), Close (C)</div>
          <div styleName="date">{date}</div>
        </div>
      );
    }
  };

  handleReloadData = () => {
    this.setState({ error: false }, () => {
      this.handleLoadData();
    });
  };

  componentDidMount() {
    this.handleLoadData();
  }

  componentDidUpdate(prevProps) {
    if (prevProps.symbol !== this.props.symbol) {
      this.handleLoadData();
    }
  }

  render() {
    const { error } = this.state;
    const { symbol, stocks } = this.props;
    const symbolPresent = symbol !== '';
    const loaded = Object.keys(stocks).includes(symbol);

    return (
      <div>
        {symbolPresent && !loaded && !error && <Loading />}
        {symbolPresent && loaded && !error && <div styleName="container">{this.renderStock()}</div>}
        {symbolPresent && error && (
          <div>
            <ErrorMessage />
            <ReloadButton onClickMethod={this.handleReloadData} />
          </div>
        )}
      </div>
    );
  }
}

export default RenderTrends;
