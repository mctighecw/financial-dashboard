const baseUrl = 'https://www.investopedia.com/terms';

const chartsInfo = {
  Aroon: { title: 'Aroon Values', url: `${baseUrl}/a/aroon.asp` },
  ADX: {
    title: 'Average Directional Movement Index',
    url: `${baseUrl}/a/adx.asp`,
  },
  BBands: { title: 'Bollinger Bands', url: `${baseUrl}/b/bollingerbands.asp` },
  MACD: {
    title: 'Moving Average Convergence/Divergence',
    url: `${baseUrl}/m/macd.asp`,
  },
  RSI: { title: 'Relative Strength Index', url: `${baseUrl}/r/rsi.asp` },
  SMA: { title: 'Simple Moving Average', url: `${baseUrl}/s/sma.asp` },
  Stochastic: {
    title: 'Stochastic Oscillator Values',
    url: `${baseUrl}/s/stochasticoscillator.asp`,
  },
};

export default chartsInfo;
