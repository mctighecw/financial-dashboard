import React from 'react';
import { post } from 'Utils/requests';
import { dateMonthDayYear, dateDayMonthYear } from 'Utils/timeFunctions';

import {
  getGraphSize,
  handleCheckUpdate,
  handleCheckIndicatorUpdate,
  redirectToLogin,
} from 'Utils/functions';

import { getStocksUrl } from 'Utils/network';

import Loading from 'Components/Loading/Loading';
import MetaDataBox from 'Components/MetaDataBox/MetaDataBox';
import ErrorMessage from 'Components/ErrorMessage/ErrorMessage';
import ReloadButton from 'Components/ReloadButton/ReloadButton';

import CandleStickGraph from 'Components/GraphTypes/CandleStickGraph/CandleStickGraph';
import CandleStickLineGraph from 'Components/GraphTypes/CandleStickLineGraph/CandleStickLineGraph';
import SecondaryLineGraph from 'Components/GraphTypes/SecondaryLineGraph/SecondaryLineGraph';
import LineBarGraph from 'Components/GraphTypes/LineBarGraph/LineBarGraph';

import chartsInfo from './chartsInfo';

import '../styles.scss';

class RenderIndicators extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      error: false,
    };
  }

  handleLoadData = () => {
    const { type, symbol, stocks, indicators } = this.props;

    if (symbol !== '') {
      const shouldUpdateData1 = handleCheckUpdate(stocks, symbol);
      const shouldUpdateData2 = handleCheckIndicatorUpdate(indicators, symbol, type);
      if (shouldUpdateData1) this.handleStocksRequest(symbol);
      if (shouldUpdateData2) this.handleIndicatorsRequest(symbol, type);
    }
  };

  handleStocksRequest = (symbol) => {
    const url = getStocksUrl('time_series');
    const data = JSON.stringify({ symbol });

    post(url, data)
      .then((res) => {
        const { meta_data, date, data } = res.data;

        this.props.onSaveStockData({
          symbol,
          date,
          data,
          metaData: meta_data,
        });
      })
      .catch((err) => {
        console.log(err);

        const setError = () => {
          this.setState({ error: true });
        };
        redirectToLogin(err, this.props.history, setError);
      });
  };

  handleIndicatorsRequest = (symbol, type) => {
    const url = getStocksUrl('tech_indicators');
    const data = JSON.stringify({ symbol, type });

    post(url, data)
      .then((res) => {
        const { data } = res.data;

        this.props.onSaveIndicatorData({
          symbol,
          data,
        });
      })
      .catch((err) => {
        console.log(err);

        const setError = () => {
          this.setState({ error: true });
        };
        redirectToLogin(err, this.props.history, setError);
      });
  };

  renderGraph = () => {
    const { user, type, symbol, stocks, indicators } = this.props;
    const graphSize = getGraphSize(user);
    const { show_graph_lines, time_format, date_format } = user.config;

    if (symbol !== '') {
      const date =
        date_format === 'month-day'
          ? dateMonthDayYear(stocks[symbol].date)
          : dateDayMonthYear(stocks[symbol].date);

      return (
        <div styleName={`graph ${graphSize}`}>
          <div styleName="top-row">
            <div styleName="title">
              <a href={chartsInfo[type].url} target="_blank">
                {chartsInfo[type].title}
              </a>
              {` — ${symbol}`}
            </div>
            <MetaDataBox data={indicators[symbol][type].meta_data} />
          </div>

          {['BBands', 'SMA'].includes(type) && (
            <CandleStickLineGraph
              stocksData={stocks[symbol]}
              indicatorsData={indicators[symbol][type]}
              showLines={show_graph_lines}
              timeFormat={time_format}
            />
          )}
          {['Aroon', 'ADX', 'RSI', 'Stochastic'].includes(type) && (
            <div>
              <CandleStickGraph
                stocksData={stocks[symbol]}
                showLines={show_graph_lines}
                timeFormat={time_format}
              />
              <SecondaryLineGraph
                date={stocks[symbol].date}
                indicatorsData={indicators[symbol][type]}
                showLines={show_graph_lines}
                timeFormat={time_format}
              />
            </div>
          )}
          {['MACD'].includes(type) && (
            <div>
              <CandleStickGraph
                stocksData={stocks[symbol]}
                showLines={show_graph_lines}
                timeFormat={time_format}
              />
              <LineBarGraph
                barField="MACD_Hist"
                date={stocks[symbol].date}
                indicatorsData={indicators[symbol][type]}
                showLines={show_graph_lines}
                timeFormat={time_format}
              />
            </div>
          )}

          <div styleName="tooltip-keys">Hover box keys: Open (O), High (H), Low (L), Close (C)</div>
          <div styleName="date">{date}</div>
        </div>
      );
    }
  };

  handleReloadData = () => {
    this.setState({ error: false }, () => {
      this.handleLoadData();
    });
  };

  componentDidMount() {
    this.handleLoadData();
  }

  componentDidUpdate(prevProps) {
    const { error } = this.state;

    if (prevProps.symbol !== this.props.symbol || prevProps.type !== this.props.type) {
      if (error) this.setState({ error: false });
      this.handleLoadData();
    }
  }

  render() {
    const { error } = this.state;
    const { type, symbol, stocks, indicators } = this.props;
    const symbolPresent = symbol !== '';
    const loaded =
      Object.keys(stocks).includes(symbol) &&
      Object.keys(indicators).includes(symbol) &&
      indicators[symbol][type];

    return (
      <div>
        {symbolPresent && !loaded && !error && <Loading />}
        {symbolPresent && loaded && !error && <div styleName="container">{this.renderGraph()}</div>}
        {symbolPresent && error && (
          <div>
            <ErrorMessage />
            <ReloadButton onClickMethod={this.handleReloadData} />
          </div>
        )}
      </div>
    );
  }
}

export default RenderIndicators;
