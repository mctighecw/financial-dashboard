import React from 'react';
import { post } from 'Utils/requests';
import { makeCryptoKey, getGraphSize, handleCheckUpdate, redirectToLogin } from 'Utils/functions';
import { getCurrenciesUrl } from 'Utils/network';

import Loading from 'Components/Loading/Loading';
import MetaDataBox from 'Components/MetaDataBox/MetaDataBox';
import LineGraph from 'Components/GraphTypes/LineGraph/LineGraph';
import ErrorMessage from 'Components/ErrorMessage/ErrorMessage';
import ReloadButton from 'Components/ReloadButton/ReloadButton';

import '../styles.scss';

class RenderCryptoCurrencies extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      error: false,
    };
  }

  handleLoadData = () => {
    const { currencies, symbolsDict } = this.props;

    if (Object.keys(symbolsDict).length > 0) {
      Object.values(symbolsDict).forEach((item) => {
        const key = makeCryptoKey(item);
        const shouldUpdateData = handleCheckUpdate(currencies, key);

        if (shouldUpdateData) this.handleCryptoCurrencyRequest(key, item);
      });
    }
  };

  handleCryptoCurrencyRequest = (key, item) => {
    const { symbol, market } = item;
    const url = getCurrenciesUrl('crypto_currencies');
    const data = JSON.stringify({ symbol, market });

    post(url, data)
      .then((res) => {
        const { meta_data, data } = res.data;

        this.props.onSaveCurrencyData({
          key,
          data,
          metaData: meta_data,
        });
      })
      .catch((err) => {
        console.log(err);

        const setError = () => {
          this.setState({ error: true });
        };
        redirectToLogin(err, this.props.history, setError);
      });
  };

  renderData = () => {
    const { symbolsDict, currencies, user } = this.props;
    const graphSize = getGraphSize(user);
    const { show_graph_lines, graph_color_scheme, time_format, date_format } = user.config;
    const divs = [];

    Object.values(symbolsDict).map((item, index) => {
      const key = makeCryptoKey(item);
      const title = item.symbol + ' — ' + item.market;

      if (currencies[key]) {
        divs.push(
          <div key={index} styleName={`graph ${graphSize}`}>
            <div styleName="top-row">
              <div styleName="title">{title}</div>
              <MetaDataBox data={currencies[key].metaData} />
            </div>
            <LineGraph
              type="currency"
              allData={currencies[key]}
              showLines={show_graph_lines}
              colorScheme={graph_color_scheme}
              timeFormat={time_format}
              dateFormat={date_format}
            />
          </div>
        );
      }
    });

    if (divs.length > 0) {
      return divs;
    } else {
      return <div styleName="none">None yet</div>;
    }
  };

  handleReloadData = () => {
    this.setState({ error: false }, () => {
      this.handleLoadData();
    });
  };

  componentDidMount() {
    this.handleLoadData();
  }

  componentDidUpdate(prevProps) {
    if (Object.keys(prevProps.symbolsDict).length !== Object.keys(this.props.symbolsDict).length) {
      this.handleLoadData();
    }
  }

  render() {
    const { error } = this.state;
    const { symbolsDict, currencies } = this.props;
    let currenciesSymbols = 0;

    Object.values(symbolsDict).forEach((item) => {
      const key = makeCryptoKey(item);

      if (Object.keys(currencies).includes(key)) {
        currenciesSymbols += 1;
      }
    });

    return (
      <div>
        {currenciesSymbols !== Object.keys(symbolsDict).length && !error && <Loading />}
        {currenciesSymbols === Object.keys(symbolsDict).length && !error && (
          <div styleName="container">{this.renderData()}</div>
        )}
        {error && (
          <div>
            <ErrorMessage />
            <ReloadButton onClickMethod={this.handleReloadData} />
          </div>
        )}
      </div>
    );
  }
}

export default RenderCryptoCurrencies;
