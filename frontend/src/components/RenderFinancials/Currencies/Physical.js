import React from 'react';
import { post } from 'Utils/requests';
import { makeCurrencyKey, getGraphSize, handleCheckUpdate, redirectToLogin } from 'Utils/functions';
import { getCurrenciesUrl } from 'Utils/network';

import Loading from 'Components/Loading/Loading';
import MetaDataBox from 'Components/MetaDataBox/MetaDataBox';
import LineGraph from 'Components/GraphTypes/LineGraph/LineGraph';
import ErrorMessage from 'Components/ErrorMessage/ErrorMessage';
import ReloadButton from 'Components/ReloadButton/ReloadButton';

import '../styles.scss';

class RenderPhysicalCurrencies extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      error: false,
    };
  }

  handleLoadData = () => {
    const { currencies, symbolsDict } = this.props;

    if (Object.keys(symbolsDict).length > 0) {
      Object.values(symbolsDict).forEach((item) => {
        const key = makeCurrencyKey(item);
        const shouldUpdateData = handleCheckUpdate(currencies, key);

        if (shouldUpdateData) this.handleCurrencyRequest(key, item);
      });
    }
  };

  handleCurrencyRequest = (key, item) => {
    const { from_symbol, to_symbol } = item;
    const url = getCurrenciesUrl('foreign_exchange');
    const data = JSON.stringify({ from_symbol, to_symbol });

    post(url, data)
      .then((res) => {
        const { meta_data, data } = res.data;

        this.props.onSaveCurrencyData({
          key,
          data,
          metaData: meta_data,
        });
      })
      .catch((err) => {
        console.log(err);

        const setError = () => {
          this.setState({ error: true });
        };
        redirectToLogin(err, this.props.history, setError);
      });
  };

  renderData = () => {
    const { symbolsDict, currencies, user } = this.props;
    const graphSize = getGraphSize(user);
    const { show_graph_lines, graph_color_scheme, time_format, date_format } = user.config;
    const divs = [];

    Object.values(symbolsDict).map((item, index) => {
      const key = makeCurrencyKey(item);
      const title = item.from_symbol + ' — ' + item.to_symbol;

      if (currencies[key]) {
        divs.push(
          <div key={index} styleName={`graph ${graphSize}`}>
            <div styleName="top-row">
              <div styleName="title">{title}</div>
              <MetaDataBox data={currencies[key].metaData} />
            </div>
            <LineGraph
              type="currency"
              allData={currencies[key]}
              showLines={show_graph_lines}
              colorScheme={graph_color_scheme}
              timeFormat={time_format}
              dateFormat={date_format}
            />
          </div>
        );
      }
    });

    if (divs.length > 0) {
      return divs;
    } else {
      return <div styleName="none">None yet</div>;
    }
  };

  handleReloadData = () => {
    this.setState({ error: false }, () => {
      this.handleLoadData();
    });
  };

  componentDidMount() {
    this.handleLoadData();
  }

  componentDidUpdate(prevProps) {
    if (Object.keys(prevProps.symbolsDict).length !== Object.keys(this.props.symbolsDict).length) {
      this.handleLoadData();
    }
  }

  render() {
    const { error } = this.state;
    const { symbolsDict, currencies } = this.props;
    let currenciesSymbols = 0;

    Object.values(symbolsDict).forEach((item) => {
      const key = makeCurrencyKey(item);

      if (Object.keys(currencies).includes(key)) {
        currenciesSymbols += 1;
      }
    });

    return (
      <div>
        {currenciesSymbols !== Object.keys(symbolsDict).length && !error && <Loading />}
        {currenciesSymbols === Object.keys(symbolsDict).length && !error && (
          <div styleName="container">{this.renderData()}</div>
        )}
        {error && (
          <div>
            <ErrorMessage />
            <ReloadButton onClickMethod={this.handleReloadData} />
          </div>
        )}
      </div>
    );
  }
}

export default RenderPhysicalCurrencies;
