import React from 'react';
import { withRouter } from 'react-router-dom';
import { post } from 'Utils/requests';
import { getAuthUrl } from 'Utils/network';
import { getErrorMessage } from 'Utils/functions';

import AuthInput from 'Components/shared/inputs/AuthInput/AuthInput';
import FullButton from 'Components/shared/buttons/Full/Full';

import DashboardIcon from 'Assets/dashboard-btc.svg';
import '../styles.scss';

class SignUp extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      username: '',
      email: '',
      password: '',
      passwordReview: '',
      error: '',
    };
  }

  handleFieldChange = (event) => {
    const { name, value } = event.target;
    this.setState({ [name]: value, error: '' });
  };

  handleInputKeyPress = (event) => {
    if (event.which === 13 || event.keyCode === 13) {
      this.handleSignUpUser();
    }
  };

  handleSignUpUser = () => {
    const { username, email, password, passwordReview } = this.state;
    const { history } = this.props;

    this.clearMessages();

    if (username !== '' && email !== '' && password !== '') {
      if (password !== passwordReview) {
        this.setState({ error: 'Your passwords do not match' });
      } else {
        const url = getAuthUrl('register');
        const data = JSON.stringify({ username, email, password });

        post(url, data)
          .then((res) => {
            history.push('/login');
          })
          .catch((err) => {
            console.log(err);
            const message = getErrorMessage(err);
            this.setState({ error: message });
          });
      }
    } else {
      this.setState({ error: 'Please fill out all fields' });
    }
  };

  handleAuthLink = () => {
    this.clearMessages();
    this.props.history.push('/login');
  };

  clearMessages = () => {
    this.setState({ error: '' });
  };

  render() {
    const { username, email, password, passwordReview, error } = this.state;

    return (
      <div styleName="container">
        <div styleName="box">
          <div styleName="left-box">
            <div styleName="content">
              <div styleName="title">
                Financial
                <br />
                Dashboard
              </div>
              <img src={DashboardIcon} alt="Dashboard" />
            </div>
          </div>

          <div styleName="right-box">
            <div styleName="content">
              <div styleName="heading">Sign Up</div>

              <AuthInput
                type="text"
                value={username}
                name="username"
                maxLength={12}
                placeholder="Username"
                onChangeMethod={this.handleFieldChange}
              />

              <AuthInput
                type="text"
                value={email}
                name="email"
                maxLength={30}
                placeholder="Email"
                onChangeMethod={this.handleFieldChange}
              />

              <AuthInput
                type="password"
                value={password}
                name="password"
                placeholder="Password"
                onChangeMethod={this.handleFieldChange}
              />

              <AuthInput
                type="password"
                value={passwordReview}
                name="passwordReview"
                placeholder="Password (review)"
                onKeyPress={this.handleInputKeyPress}
                onChangeMethod={this.handleFieldChange}
              />

              <div styleName="error">{error}</div>

              <FullButton label="Sign up" onClickMethod={this.handleSignUpUser} />

              <div styleName="link" onClick={this.handleAuthLink}>
                Go to login
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default withRouter(SignUp);
