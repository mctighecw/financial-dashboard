import React from 'react';
import { post } from 'Utils/requests';
import { getAuthUrl } from 'Utils/network';
import { getErrorMessage } from 'Utils/functions';

import AuthInput from 'Components/shared/inputs/AuthInput/AuthInput';
import FullButton from 'Components/shared/buttons/Full/Full';

import DashboardIcon from 'Assets/dashboard-btc.svg';
import '../styles.scss';

class Login extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      username: 'jsmith',
      password: 'password',
      error: '',
    };
  }

  handleFieldChange = (event) => {
    const { name, value } = event.target;
    this.setState({ [name]: value, error: '' });
  };

  handleInputKeyPress = (event) => {
    if (event.which === 13 || event.keyCode === 13) {
      this.handleLoginUser();
    }
  };

  handleLoginUser = () => {
    const { username, password } = this.state;
    const { history } = this.props;

    this.setState({ error: '' });

    if (username !== '' && password !== '') {
      const url = getAuthUrl('login');
      const data = JSON.stringify({ username, password });

      post(url, data)
        .then((res) => {
          const { data } = res;
          if (data.status === 'OK') {
            const { info, config, financials } = data;
            this.props.onSetUserInfo({ info, config, financials });
            history.push('/overview');
          } else {
            this.setState({ error: data.message });
          }
        })
        .catch((err) => {
          console.log(err);
          const message = getErrorMessage(err);
          this.setState({ error: message });
        });
    } else {
      this.setState({ error: 'Please fill out both fields' });
    }
  };

  handleAuthLink = () => {
    this.clearMessages();
    this.props.history.push('/sign-up');
  };

  clearMessages = () => {
    this.setState({ error: '' });
  };

  render() {
    const { username, password, error } = this.state;

    return (
      <div styleName="container">
        <div styleName="box">
          <div styleName="left-box">
            <div styleName="content">
              <div styleName="title">
                Financial
                <br />
                Dashboard
              </div>
              <img src={DashboardIcon} alt="Dashboard" />
            </div>
          </div>

          <div styleName="right-box">
            <div styleName="content">
              <div styleName="heading">Login</div>

              <AuthInput
                type="text"
                value={username}
                name="username"
                placeholder="Username"
                onChangeMethod={this.handleFieldChange}
              />

              <AuthInput
                type="password"
                value={password}
                name="password"
                placeholder="Password"
                onKeyPressMethod={this.handleInputKeyPress}
                onChangeMethod={this.handleFieldChange}
              />

              <div styleName="error">{error}</div>

              <FullButton label="Log in" onClickMethod={this.handleLoginUser} />

              <div styleName="link" onClick={this.handleAuthLink}>
                Go to sign up
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Login;
