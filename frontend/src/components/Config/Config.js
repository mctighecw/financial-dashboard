import React from 'react';
import { post } from 'Utils/requests';
import { getUserUrl } from 'Utils/network';
import { redirectToLogin } from 'Utils/functions';

import CheckBox from 'Components/shared/inputs/CheckBox/CheckBox';
import DropDown from 'Components/shared/inputs/DropDown/DropDown';

import graphSizes from './options/graphSizes';
import timeFormats from './options/timeFormats';
import dateFormats from './options/dateFormats';
import colors from './options/colors';
import currencies from './options/currencies';

import './styles.scss';

const Config = (props) => {
  const { history, user, onUpdateConfig } = props;

  const updateConfig = (field, value) => {
    const url = getUserUrl('config_update');
    const data = JSON.stringify({ field, value });

    post(url, data)
      .then((res) => {
        const { data } = res;
        onUpdateConfig(data.config);
      })
      .catch((err) => {
        console.log(err);
        redirectToLogin(err, history);
      });
  };

  const renderColors = (scheme) => {
    return [...Array(4).keys()].map((item, index) => (
      <div key={index} styleName="color" style={{ backgroundColor: colors[scheme - 1].data[index] }} />
    ));
  };

  return (
    <div styleName="container">
      <div styleName="heading margin-bottom">Config</div>

      <div styleName="settings margin-bottom">
        <div styleName="header">Graph Settings</div>
        <CheckBox
          label="Show volume graph"
          checked={user.config.show_volume_graph}
          onChangeMethod={(event) => updateConfig('show_volume_graph', event.target.checked)}
        />

        <CheckBox
          label="Show grid lines"
          checked={user.config.show_graph_lines}
          onChangeMethod={(event) => updateConfig('show_graph_lines', event.target.checked)}
        />

        <div styleName="dropdown-box">
          <div styleName="label">Graph size</div>
          <div styleName="dropdown">
            <DropDown
              size="normal"
              value={user.config.graph_size}
              options={graphSizes}
              onChangeMethod={(event) => updateConfig('graph_size', event.target.value)}
            />
          </div>
        </div>

        <div styleName="dropdown-box">
          <div styleName="label">Time format</div>
          <div styleName="dropdown">
            <DropDown
              size="normal"
              value={user.config.time_format}
              options={timeFormats}
              onChangeMethod={(event) => updateConfig('time_format', event.target.value)}
            />
          </div>
        </div>

        <div styleName="dropdown-box">
          <div styleName="label">Date format</div>
          <div styleName="dropdown">
            <DropDown
              size="normal"
              value={user.config.date_format}
              options={dateFormats}
              onChangeMethod={(event) => updateConfig('date_format', event.target.value)}
            />
          </div>
        </div>

        <div styleName="dropdown-box">
          <div styleName="label">Colors</div>
          <div styleName="dropdown">
            <DropDown
              size="normal"
              value={user.config.graph_color_scheme}
              options={colors}
              onChangeMethod={(event) => updateConfig('graph_color_scheme', parseInt(event.target.value, 10))}
            />
          </div>
          <div styleName="colors">{renderColors(user.config.graph_color_scheme)}</div>
        </div>
      </div>

      <div styleName="settings">
        <div styleName="header">User Settings</div>

        <div styleName="user-info">
          <div styleName="info-line">
            <span>Username: </span>
            {user.info.username}
          </div>
          <div styleName="info-line">
            <span>Email: </span>
            {user.info.email}
          </div>
        </div>

        <div styleName="dropdown-box">
          <div styleName="label">Default currency</div>
          <div styleName="dropdown">
            <DropDown
              size="long"
              value={user.config.default_currency}
              options={currencies}
              onChangeMethod={(event) => updateConfig('default_currency', event.target.value)}
            />
          </div>
        </div>
      </div>
    </div>
  );
};

export default Config;
