const cryptoCurrencies = [
  {
    value: 'BTC',
    label: 'Bitcoin (BTC)',
  },
  {
    value: 'BCH',
    label: 'Bitcoin Cash (BCH)',
  },
  {
    value: 'ETH',
    label: 'Ethereum (ETH)',
  },
  {
    value: 'XRP',
    label: 'Ripple (XRP)',
  },
  {
    value: 'EOS',
    label: 'EOS (EOS)',
  },
];

export default cryptoCurrencies;
