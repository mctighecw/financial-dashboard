const currencies = [
  {
    value: 'USD',
    label: 'US Dollar (USD)',
  },
  {
    value: 'EUR',
    label: 'Euro (EUR)',
  },
  {
    value: 'JPY',
    label: 'Japanese Yen (JPY)',
  },
  {
    value: 'GBP',
    label: 'Pound Sterling (GBP)',
  },
  {
    value: 'CAD',
    label: 'Canadian Dollar (CAD)',
  },
  {
    value: 'AUD',
    label: 'Australian Dollar (AUD)',
  },
  {
    value: 'CHF',
    label: 'Swiss Franc (CHF)',
  },
  {
    value: 'CNH',
    label: 'Chinese Renminbi (CNH)',
  },
];

export default currencies;
