const timeFormats = [
  {
    value: '24hr',
    label: '24 Hour',
  },
  {
    value: 'am-pm',
    label: 'AM PM',
  },
];

export default timeFormats;
