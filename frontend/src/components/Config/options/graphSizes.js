const graphSizes = [];

const sizes = ['small', 'medium', 'large', 'extra large'];

sizes.forEach((item, index) => {
  const label = item.charAt(0).toUpperCase() + item.slice(1);
  graphSizes.push({ value: item, label });
});

export default graphSizes;
