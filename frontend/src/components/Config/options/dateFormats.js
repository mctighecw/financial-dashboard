const dateFormats = [
  {
    value: 'month-day',
    label: 'Month Day, Year',
  },
  {
    value: 'day-month',
    label: 'Day Month Year',
  },
];

export default dateFormats;
