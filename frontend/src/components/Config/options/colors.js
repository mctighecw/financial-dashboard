import {
  GRAPH_COLORS_1,
  GRAPH_COLORS_2,
  GRAPH_COLORS_3,
  GRAPH_COLORS_4,
  GRAPH_COLORS_5,
  GRAPH_COLORS_6,
} from 'Styles/colors';

const schemes = [
  GRAPH_COLORS_1,
  GRAPH_COLORS_2,
  GRAPH_COLORS_3,
  GRAPH_COLORS_4,
  GRAPH_COLORS_5,
  GRAPH_COLORS_6,
];

const colors = [];

schemes.forEach((item, index) => {
  const value = index + 1;
  colors.push({ value, label: `Color scheme ${value}`, data: item });
});

export default colors;
