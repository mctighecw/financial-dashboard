import React from 'react';
import './styles.scss';

const TabsMenu = ({ links, selected, onClickMethod }) => {
  const renderLinks = () => {
    const result = [];

    links.map((item, index) => {
      const style = selected === item ? 'link active' : 'link';

      result.push(
        <div key={index} styleName={style} onClick={() => onClickMethod(item)}>
          {item}
        </div>
      );
    });
    return result;
  };

  return <div styleName="tabs-menu">{renderLinks()}</div>;
};

export default TabsMenu;
