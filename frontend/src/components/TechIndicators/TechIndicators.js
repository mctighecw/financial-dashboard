import React, { useState } from 'react';
import DropDown from 'Components/shared/inputs/DropDown/DropDown';
import TabsMenu from 'Components/TabsMenu/TabsMenu';
import RenderTrends from 'Containers/RenderFinancials/Trends';
import RenderIndicators from 'Containers/RenderFinancials/Indicators';
import chartsInfo from 'Components/RenderFinancials/Indicators/chartsInfo';
import './styles.scss';

const TechIndicators = (props) => {
  const { stocks } = props.user.financials;
  const [selectedStock, useSelectedStock] = useState('');
  const [selectedGraph, useSelectedGraph] = useState('Trends');
  const techIndicators = Object.keys(chartsInfo);
  const links = ['Trends', ...techIndicators];

  const renderOptions = (stocks) => {
    const result = [];

    stocks.forEach((item) => {
      result.push({ value: item, label: item });
    });
    return result;
  };

  const options = renderOptions(stocks);

  return (
    <div styleName="container">
      <div styleName="heading">Technical Indicators</div>

      <DropDown
        size="normal"
        value={selectedStock}
        placeholder="Select a stock"
        options={options}
        onChangeMethod={(event) => useSelectedStock(event.target.value)}
      />
      <br />

      <TabsMenu links={links} selected={selectedGraph} onClickMethod={(item) => useSelectedGraph(item)} />
      {selectedGraph === 'Trends' ? (
        <RenderTrends symbol={selectedStock} />
      ) : (
        <RenderIndicators type={selectedGraph} symbol={selectedStock} />
      )}
    </div>
  );
};

export default TechIndicators;
