import React from 'react';
import LoadingSpinner from 'Assets/loading-spinner.svg';
import './styles.scss';

const Loading = () => (
  <div styleName="container">
    <img src={LoadingSpinner} alt="." />
    <div styleName="message">Loading...</div>
  </div>
);

export default Loading;
