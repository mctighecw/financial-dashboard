import React, { useState } from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';

import TopBar from 'Containers/TopBar';
import SidePanel from 'Components/SidePanel/SidePanel';
import Overview from 'Containers/Overview';
import MyStocks from 'Containers/MyStocks';
import TechIndicators from 'Containers/TechIndicators';
import Currencies from 'Containers/Currencies';
import Analytics from 'Components/Analytics/Analytics';
import EditGraphs from 'Containers/EditGraphs';
import Config from 'Containers/Config';
import Info from 'Components/Info/Info';

import './styles.scss';

const Main = () => {
  const [showPanel, toggleShowPanel] = useState(false);

  const toggleSidePanel = () => {
    toggleShowPanel(!showPanel);
  };

  return (
    <div styleName="container">
      <TopBar toggleSidePanel={toggleSidePanel} />

      <div styleName="content">
        {showPanel && <SidePanel />}

        <div styleName="main">
          <Switch>
            <Route path="/overview" component={Overview} />
            <Route path="/my-stocks" component={MyStocks} />
            <Route path="/tech-indicators" component={TechIndicators} />
            <Route path="/currencies" component={Currencies} />
            {/* <Route path="/analytics" component={Analytics} /> */}
            <Route path="/edit-graphs" component={EditGraphs} />
            <Route path="/config" component={Config} />
            <Route path="/info" component={Info} />
            <Route render={() => <Redirect to="/overview" />} />
          </Switch>
        </div>
      </div>
    </div>
  );
};

export default Main;
