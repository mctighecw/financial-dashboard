import React from 'react';
import { getDateNow, dateMonthDayYear, dateDayMonthYear } from 'Utils/timeFunctions';
import RenderStocks from 'Containers/RenderFinancials/Stocks';
import './styles.scss';

const Overview = (props) => {
  const { config } = props.user;
  const overviewGraphs = config.overview_graphs;
  const now = getDateNow();
  const dateNow = config.date_format === 'month-day' ? dateMonthDayYear(now) : dateDayMonthYear(now);

  return (
    <div styleName="container">
      <div styleName="heading">Overview</div>
      <div styleName="date">{dateNow}</div>
      <RenderStocks symbols={overviewGraphs} />
    </div>
  );
};

export default Overview;
