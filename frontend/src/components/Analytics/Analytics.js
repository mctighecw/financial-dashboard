import React from 'react';
import './styles.scss';

const Analytics = () => {
  return (
    <div styleName="container">
      <div styleName="content">
        <div styleName="heading">Analytics</div>
        <div styleName="text">Data science and machine learning insights coming soon.</div>
      </div>
    </div>
  );
};

export default Analytics;
