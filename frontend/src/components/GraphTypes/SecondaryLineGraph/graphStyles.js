import { WHITE, GREY, INDICATOR1, INDICATOR2, INDICATOR3 } from 'Styles/colors';
const indicatorColors = [INDICATOR1, INDICATOR2, INDICATOR3];

const graphStyles = (index) => ({
  fill: false,
  lineTension: 0.1,
  backgroundColor: indicatorColors[index],
  borderColor: indicatorColors[index],
  borderCapStyle: 'butt',
  borderDash: [],
  borderDashOffset: 0.0,
  borderJoinStyle: 'miter',
  borderWidth: 3,
  pointBorderColor: indicatorColors[index],
  pointBackgroundColor: WHITE,
  pointBorderWidth: 1,
  pointHoverRadius: 5,
  pointHoverBackgroundColor: indicatorColors[index],
  pointHoverBorderColor: GREY,
  pointHoverBorderWidth: 2,
  pointRadius: 1,
  pointHitRadius: 10,
});

export default graphStyles;
