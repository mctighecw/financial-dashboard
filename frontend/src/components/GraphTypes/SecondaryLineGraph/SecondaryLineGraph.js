import React from 'react';
import { Line } from 'react-chartjs-2';
import { parseTime, parseIndicatorData, ensureMinLength } from 'Utils/graphFunctions';

import options from './options';
import graphStyles from './graphStyles';

const SecondaryLineGraph = (props) => {
  const { date, indicatorsData, showLines, timeFormat } = props;
  const indicatorData = indicatorsData.data;

  const renderDataSets = (parsedData) => {
    const dataSets = [];

    Object.keys(parsedData).forEach((key, index) => {
      dataSets.push({
        ...graphStyles(index),
        label: key,
        data: parsedData[key],
      });
    });
    return dataSets;
  };

  let labels = parseTime(indicatorData, timeFormat);
  let parsedIndicatorData = parseIndicatorData(indicatorData, date);

  labels = ensureMinLength(labels);
  parsedIndicatorData = ensureMinLength(parsedIndicatorData);

  const datasets = renderDataSets(parsedIndicatorData);

  const graphData = {
    labels,
    datasets,
  };

  const graphOptions = options(showLines);

  return <Line height={50} options={graphOptions} data={graphData} />;
};

export default SecondaryLineGraph;
