import { DARK_GREY, DARKER_GREY, NIGHT, OFF_WHITE } from 'Styles/colors';

const lineOptions = (showLines) => {
  const gridLines = showLines
    ? {
        display: true,
        color: DARKER_GREY,
      }
    : {
        display: false,
        color: NIGHT,
      };

  return {
    responsive: true,
    title: {
      display: false,
    },
    tooltips: {
      mode: 'label',
      callbacks: {
        title: (tooltipItems, chart) => {
          return tooltipItems[0].label;
        },
        label: (tooltipItem, chart) => {
          const dataPoint = chart.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
          return dataPoint;
        },
        labelColor: (tooltipItem, chart) => {
          const { borderColor, backgroundColor } = chart.data.datasets[tooltipItem.datasetIndex];

          return {
            borderColor,
            backgroundColor,
          };
        },
      },
    },
    hover: {
      mode: 'nearest',
      intersect: true,
    },
    legend: {
      labels: {
        fontColor: DARK_GREY,
        generateLabels: (chart) => {
          const { data } = chart;

          if (data.datasets.length > 0) {
            return data.datasets.map((item, index) => {
              return {
                ...item,
                index,
                text: item.label,
                fillStyle: item.backgroundColor,
              };
            });
          }
          return [];
        },
      },
      onClick: (event, legendItem) => {},
    },
    scales: {
      xAxes: [
        {
          display: true,
          gridLines,
          ticks: {
            fontColor: DARK_GREY,
            fontSize: 10,
            callback: (value, index, values) => {
              return '';
            },
          },
          scaleLabel: {
            display: false,
          },
        },
      ],
      yAxes: [
        {
          display: true,
          gridLines,
          ticks: {
            fontColor: DARK_GREY,
            autoSkip: true,
            maxTicksLimit: 3,
          },
          scaleLabel: {
            display: false,
          },
        },
      ],
    },
  };
};

export default lineOptions;
