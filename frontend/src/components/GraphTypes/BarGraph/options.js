import { formatLongNumber, kFormatter } from 'Utils/functions';
import { DARK_GREY, NIGHT } from 'Styles/colors';

const options = {
  maintainAspectRatio: true,
  legend: {
    labels: {
      fontColor: DARK_GREY,
    },
    onClick: (event, legendItem) => {},
  },
  tooltips: {
    callbacks: {
      label: (tooltipItem, chart) => {
        return formatLongNumber(tooltipItem.value);
      },
    },
  },
  scales: {
    xAxes: [
      {
        display: true,
        gridLines: {
          display: false,
          color: NIGHT,
        },
        ticks: {
          fontColor: DARK_GREY,
          fontSize: 10,
          callback: (value, index, values) => {
            return '';
          },
        },
        scaleLabel: {
          display: false,
        },
      },
    ],
    yAxes: [
      {
        display: true,
        gridLines: {
          display: false,
          color: NIGHT,
        },
        ticks: {
          fontColor: DARK_GREY,
          autoSkip: true,
          maxTicksLimit: 3,
          callback: (value) => {
            return kFormatter(value);
          },
        },
        scaleLabel: {
          display: false,
        },
      },
    ],
  },
};

export default options;
