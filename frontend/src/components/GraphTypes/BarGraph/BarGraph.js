import React from 'react';
import { Bar } from 'react-chartjs-2';
import { parseTime, parseGraphData, ensureMinLength } from 'Utils/graphFunctions';
import { OFF_WHITE } from 'Styles/colors';
import options from './options';

const BarGraph = (props) => {
  const { stocksData, timeFormat } = props;
  const { data } = stocksData;
  const type = 'volume';

  let labels = parseTime(data, timeFormat);
  let parsedData = parseGraphData(data, 'bar');

  labels = ensureMinLength(labels);
  parsedData = ensureMinLength(parsedData);

  const datasets = [
    {
      label: type,
      backgroundColor: OFF_WHITE,
      borderColor: OFF_WHITE,
      borderWidth: 1,
      hoverBackgroundColor: OFF_WHITE,
      hoverBorderColor: OFF_WHITE,
      data: parsedData[type],
    },
  ];

  const graphData = {
    labels,
    datasets,
  };

  return <Bar height={50} options={options} data={graphData} />;
};

export default BarGraph;
