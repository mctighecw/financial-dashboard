import React from 'react';
import ChartComponent from 'react-chartjs-2';
import 'candlestick-graph';
import { parseTime, parseTrendData, ensureMinLength } from 'Utils/graphFunctions';

import options from './options';
import graphStyles from './graphStyles';

const CandleStickGraph = (props) => {
  const { stocksData, showLines, timeFormat } = props;
  const { data } = stocksData;

  const graphOptions = options(showLines);
  let labels = parseTime(data, timeFormat);
  let parsedData = parseTrendData(data);

  labels = ensureMinLength(labels);
  parsedData = ensureMinLength(parsedData);

  const graphData = {
    labels,
    datasets: [
      {
        ...graphStyles,
        data: parsedData,
      },
    ],
  };

  return <ChartComponent type="candlestick" options={graphOptions} data={graphData} />;
};

export default CandleStickGraph;
