import { UP, DOWN, UNCHANGED } from 'Styles/colors';

const graphStyles = {
  color: {
    up: UP,
    down: DOWN,
    unchanged: UNCHANGED,
  },
  borderColor: {
    up: UNCHANGED,
    down: UNCHANGED,
    unchanged: UNCHANGED,
  },
  hoverBorderColor: {
    up: UNCHANGED,
    down: UNCHANGED,
    unchanged: UNCHANGED,
  },
};

export default graphStyles;
