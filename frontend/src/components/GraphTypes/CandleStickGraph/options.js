import { DARK_GREY, DARKER_GREY, NIGHT, UP, DOWN, UNCHANGED } from 'Styles/colors';

const options = (showLines) => {
  const gridLines = showLines
    ? {
        display: true,
        color: DARKER_GREY,
      }
    : {
        display: false,
        color: NIGHT,
      };

  return {
    tooltips: {
      mode: 'label',
      callbacks: {
        labelColor: (tooltipItem, chart) => {
          return {
            borderColor: UNCHANGED,
            backgroundColor: UNCHANGED,
          };
        },
      },
    },
    legend: {
      labels: {
        fontColor: DARK_GREY,
        generateLabels: (chart) => {
          return [
            {
              index: 0,
              text: 'Up',
              fillStyle: UP,
            },
            {
              index: 1,
              text: 'Down',
              fillStyle: DOWN,
            },
          ];
        },
      },
      onClick: (event, legendItem) => {},
    },
    scales: {
      xAxes: [
        {
          display: true,
          type: 'category',
          gridLines,
          ticks: {
            fontColor: DARK_GREY,
            fontSize: 10,
          },
          scaleLabel: {
            display: false,
          },
        },
      ],
      yAxes: [
        {
          display: true,
          gridLines,
          ticks: {
            fontColor: DARK_GREY,
          },
          scaleLabel: {
            display: false,
          },
        },
      ],
    },
  };
};

export default options;
