import React from 'react';
import ChartComponent from 'react-chartjs-2';
import 'candlestick-graph';

import { parseTime, parseTrendData, parseIndicatorData, ensureMinLength } from 'Utils/graphFunctions';
import options from './options';
import candleStickStyles from '../CandleStickGraph/graphStyles';
import lineStyles from '../SecondaryLineGraph/graphStyles';

const CandleStickLineGraph = (props) => {
  const { stocksData, indicatorsData, showLines, timeFormat } = props;
  const date = stocksData.date;
  const stockData = stocksData.data;
  const indicatorData = indicatorsData.data;

  const renderDataSets = (parsedData) => {
    const dataSets = [];

    Object.keys(parsedData).forEach((key, index) => {
      dataSets.push({
        ...lineStyles(index),
        type: 'line',
        label: key,
        data: parsedData[key],
      });
    });
    return dataSets;
  };

  let labels = parseTime(stockData, timeFormat);
  let parsedStocksData = parseTrendData(stockData);

  labels = ensureMinLength(labels);
  parsedStocksData = ensureMinLength(parsedStocksData);

  const parsedIndicatorData = parseIndicatorData(indicatorData, date);
  const lineDatasets = renderDataSets(parsedIndicatorData);

  const graphData = {
    labels,
    datasets: [
      {
        ...candleStickStyles,
        type: 'candlestick',
        label: 'candlestick',
        data: parsedStocksData,
      },
      ...lineDatasets,
    ],
  };

  const graphOptions = options(showLines);

  return <ChartComponent options={graphOptions} data={graphData} />;
};

export default CandleStickLineGraph;
