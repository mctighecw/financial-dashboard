import { DARK_GREY, DARKER_GREY, OFF_WHITE, NIGHT, UP, DOWN, UNCHANGED, INDICATOR1 } from 'Styles/colors';

const candleStickOptions = (showLines) => {
  const gridLines = showLines
    ? {
        display: true,
        color: DARKER_GREY,
      }
    : {
        display: false,
        color: NIGHT,
      };

  return {
    tooltips: {
      mode: 'label',
      callbacks: {
        title: (tooltipItems, chart) => {
          return tooltipItems[0].label;
        },
        labelColor: (tooltipItem, chart) => {
          const { borderColor, backgroundColor } = chart.data.datasets[tooltipItem.datasetIndex];

          if (typeof borderColor === 'object') {
            return {
              borderColor: UNCHANGED,
              backgroundColor: UNCHANGED,
            };
          } else {
            return {
              borderColor,
              backgroundColor,
            };
          }
        },
        label: (tooltipItem, chart) => {
          const dataPoint = chart.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];

          if (typeof dataPoint === 'object') {
            let res = '';

            Object.keys(dataPoint).forEach((item, index) => {
              const key = item.toUpperCase();
              res += `${key}: ${dataPoint[item]}`;
              if (index + 1 < Object.keys(dataPoint).length) res += ', ';
            });
            return res;
          } else {
            return dataPoint;
          }
        },
      },
    },
    legend: {
      labels: {
        fontColor: DARK_GREY,
        generateLabels: (chart) => {
          const { data } = chart;
          const labels = [];

          if (data.datasets.length > 0) {
            data.datasets.map((item, index) => {
              if (item.label === 'candlestick') {
                labels.push(
                  {
                    index: 10,
                    text: 'Up',
                    fillStyle: UP,
                  },
                  {
                    index: 11,
                    text: 'Down',
                    fillStyle: DOWN,
                  }
                );
              } else {
                labels.push({
                  ...item,
                  index,
                  text: item.label,
                  fillStyle: item.backgroundColor,
                });
              }
            });
            return labels;
          }
          return [];
        },
      },
      onClick: (event, legendItem) => {},
    },
    scales: {
      xAxes: [
        {
          display: true,
          type: 'category',
          gridLines,
          ticks: {
            fontColor: DARK_GREY,
            fontSize: 10,
          },
          scaleLabel: {
            display: false,
          },
        },
      ],
      yAxes: [
        {
          display: true,
          gridLines,
          ticks: {
            fontColor: DARK_GREY,
          },
          scaleLabel: {
            display: false,
          },
        },
      ],
    },
  };
};

export default candleStickOptions;
