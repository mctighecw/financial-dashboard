import React from 'react';
import ChartComponent from 'react-chartjs-2';

import { parseTime, parseIndicatorData, ensureMinLength } from 'Utils/graphFunctions';

import options from '../SecondaryLineGraph/options';
import lineStyles from '../SecondaryLineGraph/graphStyles';
import { BAR_COLOR } from 'Styles/colors';

const LineBarGraph = (props) => {
  const { barField, date, indicatorsData, showLines, timeFormat } = props;
  const indicatorData = indicatorsData.data;

  const renderBarDataset = (parsedData) => ({
    type: 'bar',
    label: barField,
    backgroundColor: BAR_COLOR,
    borderColor: BAR_COLOR,
    borderWidth: 1,
    hoverBackgroundColor: BAR_COLOR,
    hoverBorderColor: BAR_COLOR,
    data: parsedData[barField],
  });

  const renderLineDataSets = (parsedData) => {
    const orderedData = {};
    const dataSets = [];

    Object.keys(parsedData)
      .sort()
      .forEach((key) => {
        if (key !== barField) {
          orderedData[key] = parsedData[key];
        }
      });

    Object.keys(orderedData).forEach((key, index) => {
      dataSets.push({
        ...lineStyles(index),
        type: 'line',
        label: key,
        data: parsedData[key],
      });
    });
    return dataSets;
  };

  let labels = parseTime(indicatorData, timeFormat);
  let parsedIndicatorData = parseIndicatorData(indicatorData, date);

  labels = ensureMinLength(labels);
  parsedIndicatorData = ensureMinLength(parsedIndicatorData);

  const barDataset = renderBarDataset(parsedIndicatorData);
  const lineDatasets = renderLineDataSets(parsedIndicatorData);

  const graphData = {
    labels,
    datasets: [...lineDatasets, barDataset],
  };

  const graphOptions = options(showLines);

  return <ChartComponent height={80} options={graphOptions} data={graphData} />;
};

export default LineBarGraph;
