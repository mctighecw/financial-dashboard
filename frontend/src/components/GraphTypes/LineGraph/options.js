import { formatLongNumber } from 'Utils/functions';
import colorSchemes from 'Styles/colorSchemes';
import { DARK_GREY, DARKER_GREY, NIGHT, OFF_WHITE } from 'Styles/colors';

const options = (showLines, scheme) => {
  const colorPalette = colorSchemes[scheme - 1];

  const gridLines = showLines
    ? {
        display: true,
        color: DARKER_GREY,
      }
    : {
        display: false,
        color: NIGHT,
      };

  return {
    responsive: true,
    title: {
      display: false,
    },
    tooltips: {
      mode: 'label',
      callbacks: {
        labelColor: (tooltipItem, chart) => {
          const backgroundColor = colorPalette[tooltipItem.datasetIndex];

          return {
            borderColor: OFF_WHITE,
            backgroundColor,
          };
        },
        label: (tooltipItem, chart) => {
          return formatLongNumber(tooltipItem.value, true);
        },
      },
    },
    hover: {
      mode: 'nearest',
      intersect: true,
    },
    legend: {
      labels: {
        fontColor: DARK_GREY,
        generateLabels: (chart) => {
          const { data } = chart;

          if (data.datasets.length > 0) {
            return data.datasets.map((item, index) => {
              return {
                ...item,
                index,
                text: item.label,
                fillStyle: item.backgroundColor,
              };
            });
          }
          return [];
        },
      },
      onClick: (event, legendItem) => {},
    },
    scales: {
      xAxes: [
        {
          display: true,
          gridLines,
          ticks: {
            fontColor: DARK_GREY,
            fontSize: 10,
          },
          scaleLabel: {
            display: false,
          },
        },
      ],
      yAxes: [
        {
          display: true,
          gridLines,
          ticks: {
            fontColor: DARK_GREY,
          },
          scaleLabel: {
            display: false,
          },
        },
      ],
    },
  };
};

export default options;
