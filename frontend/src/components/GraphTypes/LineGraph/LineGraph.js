import React from 'react';
import { Line } from 'react-chartjs-2';

import {
  parseDate,
  parseTime,
  parseCurrencyData,
  parseGraphData,
  ensureMinLength,
  renderDataSets,
} from 'Utils/graphFunctions';

import options from './options';

const LineGraph = (props) => {
  const { type, allData, showLines, colorScheme, timeFormat, dateFormat } = props;
  const { data } = allData;

  let labels = type === 'currency' ? parseDate(data, dateFormat) : parseTime(data, timeFormat);
  let parsedData = type === 'currency' ? parseCurrencyData(data) : parseGraphData(data, 'line');

  labels = ensureMinLength(labels);
  parsedData = ensureMinLength(parsedData);

  const datasets = renderDataSets(parsedData, colorScheme);

  const graphData = {
    labels,
    datasets,
  };

  const graphOptions = options(showLines, colorScheme);

  return <Line options={graphOptions} data={graphData} />;
};

export default LineGraph;
