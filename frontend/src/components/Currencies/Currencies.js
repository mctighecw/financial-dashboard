import React, { useState } from 'react';
import TabsMenu from 'Components/TabsMenu/TabsMenu';
import RenderPhysicalCurrencies from 'Containers/RenderFinancials/Currencies/Physical';
import RenderCryptocurrencies from 'Containers/RenderFinancials/Currencies/Crypto';
import './styles.scss';

const Currencies = (props) => {
  const [selected, useSelected] = useState('physical');
  const { currencies, crypto_currencies } = props.user.financials;
  const links = ['physical', 'crypto'];

  return (
    <div styleName="container">
      <div styleName="heading">Currencies</div>
      <TabsMenu links={links} selected={selected} onClickMethod={(item) => useSelected(item)} />
      {selected === 'physical' ? (
        <RenderPhysicalCurrencies symbolsDict={currencies} />
      ) : (
        <RenderCryptocurrencies symbolsDict={crypto_currencies} />
      )}
    </div>
  );
};

export default Currencies;
