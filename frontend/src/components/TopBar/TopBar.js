import React from 'react';
import { get } from 'Utils/requests';
import { getAuthUrl } from 'Utils/network';

import MenuIcon from 'Assets/menu.svg';
import UserIcon from 'Assets/user.svg';
import LogOutIcon from 'Assets/logout.svg';

import './styles.scss';

const TopBar = ({ user, history, onClearAllInfo, toggleSidePanel }) => {
  const handleLogoutUser = () => {
    const url = getAuthUrl('logout');

    get(url)
      .then((res) => {
        onClearAllInfo();
        history.push('/login');
      })
      .catch((err) => {
        console.log(err);
      });
  };

  return (
    <div styleName="container">
      <div>
        <img src={MenuIcon} alt="Menu" onClick={toggleSidePanel} />
      </div>
      <div styleName="center">
        <div styleName="title">Financial Dashboard</div>
      </div>
      <div styleName="right">
        <div styleName="user">
          <img src={UserIcon} alt="User" />
          <div styleName="username">{user.info.username}</div>
        </div>
        <img src={LogOutIcon} alt="Logout" styleName="logout" onClick={handleLogoutUser} />
      </div>
    </div>
  );
};

export default TopBar;
