import {
  GRAPH_COLORS_1,
  GRAPH_COLORS_2,
  GRAPH_COLORS_3,
  GRAPH_COLORS_4,
  GRAPH_COLORS_5,
  GRAPH_COLORS_6,
} from './colors';

const colorSchemes = [
  GRAPH_COLORS_1,
  GRAPH_COLORS_2,
  GRAPH_COLORS_3,
  GRAPH_COLORS_4,
  GRAPH_COLORS_5,
  GRAPH_COLORS_6,
];

export default colorSchemes;
