// General colors
export const WHITE = '#FFF';
export const OFF_WHITE = '#F2F2F2';
export const GREY = '#DCDCDC';
export const DARK_GREY = '#9A9A9A';
export const DARKER_GREY = '#2E2E2E';
export const NIGHT = '#1B1B1B';

// Candlestick graph colors
export const UP = '#32CD32';
export const DOWN = '#EE7631';
export const UNCHANGED = '#999';

// indicators
export const INDICATOR1 = '#FF1919';
export const INDICATOR2 = '#84B3E7';
export const INDICATOR3 = '#954BFF';
export const BAR_COLOR = UNCHANGED;

// Line graph colors
const GC1_1 = '#4BC0C0';
const GC1_2 = '#FF6384';
const GC1_3 = '#36A2EB';
const GC1_4 = '#FFCE56';

const GC2_1 = '#75D5FD';
const GC2_2 = '#B76CFD';
const GC2_3 = '#FF2281';
const GC2_4 = '#011FFD';

const GC3_1 = '#FEC763';
const GC3_2 = '#EA55B1';
const GC3_3 = '#A992FA';
const GC3_4 = '#3B55CE';

const GC4_1 = '#3B27BA';
const GC4_2 = '#E847AE';
const GC4_3 = '#13CA91';
const GC4_4 = '#FF9472';

const GC5_1 = '#C6BDEA';
const GC5_2 = '#FFAA01';
const GC5_3 = '#12B296';
const GC5_4 = '#027A9F';

const GC6_1 = '#F78888';
const GC6_2 = '#ECECEC';
const GC6_3 = '#F3D250';
const GC6_4 = '#90CCF4';

export const GRAPH_COLORS_1 = [GC1_1, GC1_2, GC1_3, GC1_4];
export const GRAPH_COLORS_2 = [GC2_1, GC2_2, GC2_3, GC2_4];
export const GRAPH_COLORS_3 = [GC3_1, GC3_2, GC3_3, GC3_4];
export const GRAPH_COLORS_4 = [GC4_1, GC4_2, GC4_3, GC4_4];
export const GRAPH_COLORS_5 = [GC5_1, GC5_2, GC5_3, GC5_4];
export const GRAPH_COLORS_6 = [GC6_1, GC6_2, GC6_3, GC6_4];
