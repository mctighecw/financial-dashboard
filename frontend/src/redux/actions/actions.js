import * as types from './actionTypes';

// user
export const setUserInfo = (payload) => ({
  type: types.SET_USER_INFO,
  payload,
});

export const clearUserInfo = () => ({
  type: types.CLEAR_USER_INFO,
});

export const updateConfig = (payload) => ({
  type: types.UPDATE_CONFIG,
  payload,
});

export const updateStocks = (payload) => ({
  type: types.UPDATE_STOCKS,
  payload,
});

export const updateCurrencies = (payload) => ({
  type: types.UPDATE_CURRENCIES,
  payload,
});

export const updateCryptoCurrencies = (payload) => ({
  type: types.UPDATE_CRYPTO_CURRENCIES,
  payload,
});

// stocks
export const saveStockData = (payload) => ({
  type: types.SAVE_STOCK_DATA,
  payload,
});

export const clearStockData = () => ({
  type: types.CLEAR_STOCK_DATA,
});

// currencies
export const saveCurrencyData = (payload) => ({
  type: types.SAVE_CURRENCY_DATA,
  payload,
});

export const clearCurrencyData = () => ({
  type: types.CLEAR_CURRENCY_DATA,
});

// indicators
export const saveIndicatorData = (payload) => ({
  type: types.SAVE_INDICATOR_DATA,
  payload,
});

export const clearIndicatorData = () => ({
  type: types.CLEAR_INDICATOR_DATA,
});
