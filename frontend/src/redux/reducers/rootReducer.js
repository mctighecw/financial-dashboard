import { combineReducers } from 'redux';
import user from './user';
import stocks from './stocks';
import currencies from './currencies';
import indicators from './indicators';

const rootReducer = combineReducers({
  user,
  stocks,
  currencies,
  indicators,
});

export default rootReducer;
