import * as types from 'Redux/actions/actionTypes';
import { getDateStringNow } from 'Utils/timeFunctions';

const initialState = {};

const stocks = (state = initialState, action = {}) => {
  switch (action.type) {
    case types.SAVE_STOCK_DATA: {
      const currentDateTime = getDateStringNow();
      return {
        ...state,
        [action.payload.symbol]: {
          timestamp: currentDateTime,
          date: action.payload.date,
          metaData: action.payload.metaData,
          data: action.payload.data,
        },
      };
    }
    case types.CLEAR_STOCK_DATA: {
      return initialState;
    }
    default:
      return state;
  }
};

export default stocks;
