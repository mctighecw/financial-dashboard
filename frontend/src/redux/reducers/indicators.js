import * as types from 'Redux/actions/actionTypes';
import { getDateStringNow } from 'Utils/timeFunctions';

const initialState = {};

const indicators = (state = initialState, action = {}) => {
  switch (action.type) {
    case types.SAVE_INDICATOR_DATA: {
      const currentDateTime = getDateStringNow();
      const key = Object.keys(action.payload.data)[0];
      const updatedData = { [key]: { timestamp: currentDateTime, ...action.payload.data[key] } };

      return {
        ...state,
        [action.payload.symbol]: {
          ...state[action.payload.symbol],
          ...updatedData,
        },
      };
    }
    case types.CLEAR_INDICATOR_DATA: {
      return initialState;
    }
    default:
      return state;
  }
};

export default indicators;
