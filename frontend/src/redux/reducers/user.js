import * as types from 'Redux/actions/actionTypes';

const initialState = {
  info: {
    username: '',
    email: '',
  },
  config: {
    show_volume_graph: true,
    show_graph_lines: false,
    graph_size: 'medium',
    graph_color_scheme: 1,
    time_format: '24hr',
    date_format: 'month-day',
    overview_graphs: [],
    default_currency: '',
  },
  financials: {
    stocks: [],
    currencies: [],
    crypto_currencies: [],
  },
};

const user = (state = initialState, action = {}) => {
  switch (action.type) {
    case types.SET_USER_INFO: {
      return {
        info: action.payload.info,
        config: action.payload.config,
        financials: action.payload.financials,
      };
    }
    case types.UPDATE_CONFIG:
      return {
        ...state,
        config: action.payload,
      };
    case types.UPDATE_STOCKS:
      return {
        ...state,
        financials: {
          ...state.financials,
          stocks: action.payload,
        },
      };
    case types.UPDATE_CURRENCIES:
      return {
        ...state,
        financials: {
          ...state.financials,
          currencies: action.payload,
        },
      };
    case types.UPDATE_CRYPTO_CURRENCIES:
      return {
        ...state,
        financials: {
          ...state.financials,
          crypto_currencies: action.payload,
        },
      };
    case types.CLEAR_USER_INFO: {
      return initialState;
    }
    default:
      return state;
  }
};

export default user;
