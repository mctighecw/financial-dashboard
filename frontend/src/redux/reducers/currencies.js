import * as types from 'Redux/actions/actionTypes';
import { getDateStringNow } from 'Utils/timeFunctions';

const initialState = {};

const currencies = (state = initialState, action = {}) => {
  switch (action.type) {
    case types.SAVE_CURRENCY_DATA: {
      const currentDateTime = getDateStringNow();
      return {
        ...state,
        [action.payload.key]: {
          timestamp: currentDateTime,
          metaData: action.payload.metaData,
          data: action.payload.data,
        },
      };
    }
    case types.CLEAR_CURRENCY_DATA: {
      return initialState;
    }
    default:
      return state;
  }
};

export default currencies;
