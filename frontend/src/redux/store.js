import { createStore, applyMiddleware } from 'redux';
import logger from 'redux-logger';
import rootReducer from 'Redux/reducers/rootReducer';

const middlewares = applyMiddleware(logger);

const store =
  process.env.NODE_ENV === 'development' ? createStore(rootReducer, middlewares) : createStore(rootReducer);

export default store;
