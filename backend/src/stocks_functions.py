"""
See documentation:
https://www.alphavantage.co/documentation/
https://alpha-vantage.readthedocs.io/en/latest/
https://github.com/RomelTorres/alpha_vantage
"""

from alpha_vantage.timeseries import TimeSeries
from alpha_vantage.foreignexchange import ForeignExchange
from alpha_vantage.cryptocurrencies import CryptoCurrencies
from alpha_vantage.techindicators import TechIndicators

from src.helpers import order_bbands
from src.env_config import AV_KEY

"""
Function arguments:

- symbol: the stock's unique symbol
    (e.g. 'AAPL' for Apple, Inc.)

- format: data output format
    ('json', 'csv', 'pandas'; default: 'json')

- interval: time interval
    (possible: '1min', '5min', '15min', '30min', '60min',
    'daily', 'weekly', 'monthly'; default: '15min')

- market: currency
    (e.g. 'USD', 'EUR')
"""


def get_time_series(symbol):
    """
    Gets a stock's time series data.
    Returns intraday time series (timestamp, open, high, low, close, volume) of the equity specified.
    """
    format = 'json'
    ts = TimeSeries(key=AV_KEY, output_format=format)

    data, meta_data = ts.get_intraday(
        symbol=symbol,
        interval='5min',
        outputsize='compact'
    )
    return data, meta_data


def get_foreign_exchange(from_symbol, to_symbol):
    """
    Gets currency foreign exchange rate data.
    Returns the realtime exchange rate for any pair of digital or physical currency.
    Note: must specify two currencies: from_symbol and to_symbol.
    """
    cc = ForeignExchange(key=AV_KEY)

    data, meta_data = cc.get_currency_exchange_daily(
        from_symbol=from_symbol,
        to_symbol=to_symbol,
        outputsize='compact'
    )
    return data, meta_data


def get_crypto_currencies(symbol, market):
    """
    Gets a cryptocurrency's data.
    Returns the monthly historical time series for a digital currency traded on a specific market,
    refreshed daily at midnight (UTC).
    Prices and volumes are quoted in both the market-specific currency and USD.
    Note: symbol must be a cryptocurrency (e.g. 'BTC')
    """
    format = 'json'
    cc = CryptoCurrencies(key=AV_KEY, output_format=format)

    data, meta_data = cc.get_digital_currency_daily(
        symbol=symbol,
        market=market
    )
    return data, meta_data


def get_tech_indicators(symbol, type):
    """
    Gets some technical indicator data for a particular stock:
        - Aroon values (Aroon)
        - Average directional movement index (ADX)
        - Bollinger Bands (BBands)
        - Moving average convergence/divergence (MACD)
        - Relative strength index (RSI)
        - Simple moving average (SMA)
        - Stochastic oscillator values (Stochastic)
    Technical indicator values are updated realtime: the latest data point is derived from the
    current trading day of a given equity or currency exchange pair.
    """
    format = 'json'
    interval = '5min'
    time_period = 20
    series_type = 'close'
    ti = TechIndicators(key=AV_KEY, output_format=format)

    if type == "Aroon":
        data, meta_data = ti.get_aroon(
            symbol=symbol,
            interval=interval,
            time_period=20,
            series_type=series_type
        )
        return data, meta_data

    elif type == "ADX":
        data, meta_data = ti.get_adx(
            symbol=symbol,
            interval=interval,
            time_period=20
        )
        return data, meta_data

    elif type == "BBands":
        data, meta_data = ti.get_bbands(
            symbol=symbol,
            interval=interval,
            time_period=time_period,
            series_type=series_type
        )
        ordered_data = order_bbands(data)
        return ordered_data, meta_data

    elif type == "MACD":
        data, meta_data = ti.get_macd(
            symbol=symbol,
            interval=interval,
            series_type=series_type
        )
        return data, meta_data

    elif type == "RSI":
        data, meta_data = ti.get_rsi(
            symbol=symbol,
            interval=interval,
            time_period=time_period,
            series_type=series_type
        )
        return data, meta_data


    elif type == "SMA":
        data, meta_data = ti.get_sma(
            symbol=symbol,
            interval=interval,
            time_period=time_period,
            series_type=series_type
        )
        return data, meta_data


    elif type == "Stochastic":
        data, meta_data = ti.get_stoch(
            symbol=symbol,
            interval=interval
        )
        return data, meta_data

    else:
        print("Not a valid type")
        return {}, {}
