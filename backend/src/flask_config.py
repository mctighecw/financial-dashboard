import logging

from src.env_config import (FLASK_ENV, DB_HOST_PROD, DB_HOST_DEV,
    DB_AUTH, DB_NAME, DB_USER, DB_PASSWORD)


if FLASK_ENV == 'development':
    LOG_LEVEL = logging.DEBUG
    DB_HOST = DB_HOST_DEV
else:
    LOG_LEVEL = logging.INFO
    DB_HOST = DB_HOST_PROD


MONGODB_SETTINGS = {
    'db': DB_NAME,
    'username': DB_USER,
    'password': DB_PASSWORD,
    'host': DB_HOST,
    'authentication_source': DB_AUTH
}
