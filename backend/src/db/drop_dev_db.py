import pymongo

client = pymongo.MongoClient(
    'localhost',
    port=27017,
    username='admin',
    password='mypassword',
    authSource='admin')

print('Dropping database collections...')

db = client.get_database('financial')
users = db.users
config = db.config
data = db.data

users.drop()
config.drop()
data.drop()
