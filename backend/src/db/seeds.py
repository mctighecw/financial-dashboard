users_data = [{
    'username': 'jsmith',
    'email': 'john@smith.com',
    'password': 'password'
},
{
    'username': 'swilliams',
    'email': 'susan@williams.com',
    'password': 'password'
},
{
    'username': 'sjones',
    'email': 'sam@jones.com',
    'password': 'password'
}]

financial_data =[{
    'user': 'jsmith',
    'stocks': ['AAPL', 'AMZN'],
    'currencies': [
        {'from_symbol': 'USD', 'to_symbol': 'EUR'},
        {'from_symbol': 'USD', 'to_symbol': 'CHF'}
    ],
    'crypto_currencies': [
        {'symbol': 'BTC', 'market': 'USD'}
    ]
},
{
    'user': 'swilliams',
    'stocks': ['NKE', 'MCD'],
    'currencies': [
        {'from_symbol': 'USD', 'to_symbol': 'EUR'},
        {'from_symbol': 'USD', 'to_symbol': 'GBP'}
    ],
    'crypto_currencies': [
        {'symbol': 'BTC', 'market': 'USD'}
    ]
},
{
    'user': 'sjones',
    'stocks': ['NFLX', 'SBUX'],
    'currencies': [
        {'from_symbol': 'EUR', 'to_symbol': 'JPY'},
        {'from_symbol': 'EUR', 'to_symbol': 'CNY'}
    ],
    'crypto_currencies': [
        {'symbol': 'BTC', 'market': 'USD'}
    ]
}]
