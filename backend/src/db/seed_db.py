import os
import sys

root_dir = os.path.abspath(os.curdir)
sys.path.append(root_dir)

from flask_bcrypt import Bcrypt
from src.models import User, Config, Data
from seeds import users_data, financial_data

bcrypt = Bcrypt()

def seed_db():
    """
    Seeds database with initial data.
    """
    for item in users_data:
        print(item['username'])
        password_hash = bcrypt.generate_password_hash(item['password']).decode('utf-8')
        new_user = User(
            username=item['username'],
            email=item['email'],
            password=password_hash
        )
        new_config = Config(user=item['username'])
        new_user.save()
        new_config.save()

    for item in financial_data:
        print(item['stocks'])
        new_stocks = Data(
            user=item['user'],
            stocks=item['stocks'],
            currencies=item['currencies'],
            crypto_currencies=item['crypto_currencies']
        )
        new_stocks.save()
