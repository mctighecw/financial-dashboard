import logging
import os

from flask import Flask
from flask_cors import CORS
from flask_mongoengine import MongoEngine
from flask_jwt_extended import JWTManager

from src import flask_config
from src.env_config import SECRET_KEY

from src.routes.auth import auth
from src.routes.user import user
from src.routes.stocks import stocks
from src.routes.currencies import currencies


def create_app():
    app = Flask(__name__)
    app.config.from_object(flask_config)

    FLASK_ENV = os.getenv('FLASK_ENV')

    if FLASK_ENV == 'development':
        CORS(app, supports_credentials=True)
        secure_cookies = False
    else:
        secure_cookies = True

    logging.basicConfig(format='%(asctime)s %(levelname)s [%(module)s %(lineno)d] %(message)s',
                        level=app.config['LOG_LEVEL'])

    db = MongoEngine(app)

    app.config["JWT_TOKEN_LOCATION"] = ["cookies"]
    app.config["JWT_COOKIE_SECURE"] = secure_cookies
    app.config["JWT_ACCESS_COOKIE_PATH"] = "/api/"
    app.config["JWT_REFRESH_COOKIE_PATH"] = "/api/auth/refresh"
    app.config["JWT_COOKIE_CSRF_PROTECT"] = False
    app.config["JWT_SECRET_KEY"] = SECRET_KEY

    jwt = JWTManager(app)

    app.register_blueprint(auth, url_prefix='/api/auth')
    app.register_blueprint(user, url_prefix='/api/user')
    app.register_blueprint(stocks, url_prefix='/api/stocks')
    app.register_blueprint(currencies, url_prefix='/api/currencies')

    return app
