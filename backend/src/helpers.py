from datetime import datetime, timedelta
from collections import OrderedDict
from src.constants import year_month_day


def get_todays_date():
    """
    Calculates and returns today's date in YYYY-MM-DD format.
    """
    today = datetime.today().strftime(year_month_day)
    return today


def get_one_day_earlier(date):
    """
    Calculates and returns one day earlier than given date,
    in YYYY-MM-DD format.
    """
    date_obj = datetime.strptime(date, year_month_day).date()
    day_before = (date_obj - timedelta(1)).strftime(year_month_day)
    return day_before


def filter_data_by_date(date, data):
    """
    Filters data dictionary by key with given date.
    """
    res = {k:v for k,v in data.items() if k.startswith(date)}
    return res


def filter_latest_data(data):
    """
    Gets latest data by date, starting with today's date and going back
    one day at a time. Maximum 10 iterations before the loop stops.
    """
    date = get_todays_date()
    latest_data = filter_data_by_date(date, data)

    if bool(data):
        count = 0

        while not bool(latest_data):
            date = get_one_day_earlier(date)
            latest_data = filter_data_by_date(date, data)

            if count == 10:
                break
            count += 1

    return date, latest_data


def sort_by_key(dict):
    """
    Sorts data dictionary by date/time key, from earlier to later.
    """
    sorted_dict = OrderedDict(sorted(dict.items(), key=lambda t: t[0]))
    return sorted_dict


def organize_stocks_keys(data):
    """
    Organizes stocks data, separating the fields for the line graphs and the bar
    graphs. Simplifies the keys ("open" instead of "1. open").
    """
    res = {}

    for k, v in data.items():
        res[k] = {}
        res[k]["line"] = {x.split(" ")[1]:y for x,y in v.items() if x.split(" ")[1] != "volume"}
        res[k]["bar"] = {x.split(" ")[1]:y for x,y in v.items() if x.split(" ")[1] == "volume"}

    return res


def filtered_stock_suggestions(data):
    """
    Filters stocks suggestions data, cleaning up keys and removing
    extra key-value pairs.
    """
    res = []

    for i in data:
        j = {}
        j["symbol"] = i["1. symbol"]
        j["name"] = i["2. name"]
        res.append(j)

    return res


def organize_currency_keys(data):
    """
    Simplifies the currency keys ("open" instead of "1a. open").
    """
    res = {}

    for k, v in data.items():
        res[k] = {x.split(" ")[1]:y for x,y in v.items()}

    return res


def filter_crypto_currency_data(data):
    """
    Filters cryptocurrency data, removing the extra key-value pairs
    ("USD", marked with "b"). Simplifies the keys ("open" instead
    of "1a. open (USD)").
    """
    res = {}

    for k, v in data.items():
        res[k] = {x.split(" ")[1]:y for x,y in v.items() if x.startswith("a", 1, 2)}

    return res


def order_bbands(data):
    """
    Orders the dictionary of Bollinger Bands, for consistency in displaying data lines.
    """
    res = {}

    for k, v in data.items():
        res[k] = {}
        res[k]["Real Middle Band"] = v["Real Middle Band"]
        res[k]["Real Lower Band"] = v["Real Lower Band"]
        res[k]["Real Upper Band"] = v["Real Upper Band"]

    return res


def get_user_info(user):
    """
    Returns the user's info as a dictionary.
    """
    res = {
        "username": user.username,
        "email": user.email
    }
    return res


def get_user_config(config):
    """
    Returns the user's config as a dictionary.
    """
    res = {
        "show_volume_graph": config.show_volume_graph,
        "show_graph_lines": config.show_graph_lines,
        "graph_size": config.graph_size,
        "graph_color_scheme": config.graph_color_scheme,
        "time_format": config.time_format,
        "date_format": config.date_format,
        "overview_graphs": config.overview_graphs,
        "default_currency": config.default_currency
    }
    return res


def get_user_financials(data):
    """
    Returns the user's financial data (stocks and currencies)
    as a dictionary.
    """
    res = {
        "stocks": data.stocks,
        "currencies": data.currencies,
        "crypto_currencies": data.crypto_currencies
    }
    return res
