from datetime import datetime, timezone

from mongoengine import Document, EmbeddedDocument, EmbeddedDocumentField
from mongoengine.fields import (StringField, IntField, BooleanField,
                                ListField, DateTimeField)


class CurrencyDict(EmbeddedDocument):
    """
    Dictionary with currency fields for Data field 'currencies'.
    """
    from_symbol = StringField()
    to_symbol = StringField()


class CryptoCurrencyDict(EmbeddedDocument):
    """
    Dictionary with currency fields for Data field 'crypto_currencies'.
    """
    symbol = StringField()
    market = StringField()


class User(Document):
    meta = { 'collection': 'users' }

    username = StringField()
    email = StringField()
    password = StringField()
    registered_at = DateTimeField(default=datetime.now(timezone.utc).astimezone())


class Config(Document):
    meta = { 'collection': 'config' }

    show_volume_graph = BooleanField(default=True)
    show_graph_lines = BooleanField(default=False)
    graph_size = StringField(default='medium')
    graph_color_scheme = IntField(default=1)
    time_format = StringField(default='24hr')
    date_format = StringField(default='month-day')
    overview_graphs = ListField(StringField(), default=lambda: ['NDAQ'])
    default_currency = StringField(default='USD')
    user = StringField()
    modified_at = DateTimeField(default=datetime.now(timezone.utc).astimezone())


class Data(Document):
    meta = { 'collection': 'data' }

    stocks = ListField(StringField())
    currencies = ListField(EmbeddedDocumentField(CurrencyDict))
    crypto_currencies = ListField(EmbeddedDocumentField(CryptoCurrencyDict))
    user = StringField()
    modified_at = DateTimeField(default=datetime.now(timezone.utc).astimezone())
