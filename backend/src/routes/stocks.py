import json
import requests
import datetime

from flask import request, Blueprint, Response
from flask_cors import CORS
from flask_jwt_extended import jwt_required

from src.helpers import filtered_stock_suggestions, filter_latest_data, organize_stocks_keys, sort_by_key
from src.stocks_functions import get_time_series, get_tech_indicators
from src.constants import default_headers
from src.env_config import AV_KEY


stocks = Blueprint("stocks", __name__)
CORS(stocks, resources=r"/api/stocks/*")


@stocks.before_request
@jwt_required
def before_request():
    print("User is authorized")


@stocks.route("/symbol_search", methods=["POST"])
def symbol_search():
    """
    Stock symbols search API, which returns stock symbol and name suggestions based on
    user search term entered.
    """
    try:
        req = request.get_json(force=True, silent=False)
        search_term = req["search_term"]
        url = f"https://www.alphavantage.co/query?function=SYMBOL_SEARCH&keywords={search_term}&apikey={AV_KEY}"
        raw = requests.get(url)
        content = raw.content.decode('ascii')
        json_data = json.loads(content)
        best_matches = json_data["bestMatches"]
        search_data = filtered_stock_suggestions(best_matches)

        res = {"status": "OK", "message": "Stock symbols data retrieved successfully", "search_data": search_data}
        return Response(json.dumps(res)), 200, default_headers

    except:
        res = {"status": "Error", "message": "There was an error retrieving stock symbols data"}
        return Response(json.dumps(res)), 400, default_headers


@stocks.route("/time_series", methods=["POST"])
def time_series():
    """
    Stocks API time series request.
    """
    try:
        req = request.get_json(force=True, silent=False)
        symbol = req["symbol"]
        data, meta_data = get_time_series(symbol)
        date, latest_data = filter_latest_data(data)

        if bool(latest_data):
            organized_dict = organize_stocks_keys(latest_data)
            sorted_data = sort_by_key(organized_dict)

            res = {"status": "OK", "message": "Time series data retrieved successfully", "date": date, "meta_data": meta_data, "data": sorted_data}
            return Response(json.dumps(res)), 200, default_headers

        else:
            res = {"status": "Error", "message": "No time series data were received"}
            return Response(json.dumps(res)), 400, default_headers

    except:
        res = {"status": "Error", "message": "There was an error retrieving time series data"}
        return Response(json.dumps(res)), 400, default_headers


@stocks.route("/tech_indicators", methods=["POST"])
def tech_indicators():
    """
    Stocks API tech indicators request.
    """
    try:
        req = request.get_json(force=True, silent=False)
        symbol = req["symbol"]
        type = req["type"]
        data, meta_data = get_tech_indicators(symbol, type)
        date, latest_data = filter_latest_data(data)

        res_data = {}

        if bool(latest_data):
            sorted_data = sort_by_key(latest_data)
            res_data[type] = {"meta_data": meta_data, "data": sorted_data}

            res = {"status": "OK", "message": "Tech indicators data retrieved successfully",  "date": date, "data": res_data}
            return Response(json.dumps(res)), 200, default_headers

        else:
            res = {"status": "Error", "message": "No tech indicators data were received"}
            return Response(json.dumps(res)), 400, default_headers

    except:
        res = {"status": "Error", "message": "There was an error retrieving tech indicators data"}
        return Response(json.dumps(res)), 400, default_headers
