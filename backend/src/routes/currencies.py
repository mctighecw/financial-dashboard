import json

from flask import request, Blueprint, Response
from flask_cors import CORS
from flask_jwt_extended import jwt_required

from src.helpers import sort_by_key, organize_currency_keys, filter_crypto_currency_data
from src.constants import default_headers
from src.stocks_functions import get_foreign_exchange, get_crypto_currencies

currencies = Blueprint("currencies", __name__)
CORS(currencies, resources=r"/api/currencies/*")


@currencies.before_request
@jwt_required
def before_request():
    print("User is authorized")


@currencies.route("/foreign_exchange", methods=["POST"])
def foreign_exchange():
    """
    Currencies API foreign exchange request.
    """
    try:
        req = request.get_json(force=True, silent=False)
        from_symbol = req["from_symbol"]
        to_symbol = req["to_symbol"]
        data, meta_data = get_foreign_exchange(from_symbol, to_symbol)
        organized_keys = organize_currency_keys(data)
        sorted_data = sort_by_key(organized_keys)

        res = {"status": "OK", "message": "Foreign exchange data retrieved successfully", "meta_data": meta_data, "data": sorted_data}
        return Response(json.dumps(res)), 200, default_headers

    except:
        res = {"status": "Error", "message": "There was an error retrieving foreign exchange data"}
        return Response(json.dumps(res)), 400, default_headers


@currencies.route("/crypto_currencies", methods=["POST"])
def crypto_currencies():
    """
    Currencies API cryptocurrencies request.
    """
    try:
        req = request.get_json(force=True, silent=False)
        symbol = req["symbol"]
        market = req["market"]
        data, meta_data = get_crypto_currencies(symbol, market)
        filtered_data = filter_crypto_currency_data(data)
        sorted_data = sort_by_key(filtered_data)

        res = {"status": "OK", "message": "Cryptocurrencies data retrieved successfully", "meta_data": meta_data, "data": sorted_data}
        return Response(json.dumps(res)), 200, default_headers

    except:
        res = {"status": "Error", "message": "There was an error retrieving cryptocurrencies data"}
        return Response(json.dumps(res)), 400, default_headers
