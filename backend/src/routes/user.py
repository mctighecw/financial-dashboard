import json
from datetime import datetime, timezone

from flask import request, Blueprint, Response, jsonify
from flask_cors import CORS
from flask_jwt_extended import jwt_required, get_jwt_identity

from src.models import (Config as ConfigModel,
                        Data as DataModel,
                        CurrencyDict,
                        CryptoCurrencyDict)

from src.helpers import get_user_config
from src.constants import default_headers

user = Blueprint("user", __name__)
CORS(user, resources=r"/api/user/*")


@user.before_request
@jwt_required
def before_request():
    print("User is authorized")


@user.route("/config_update", methods=["POST"])
def config_update():
    """
    Updates the user's config.
    """
    user = get_jwt_identity()
    req = request.get_json(force=True, silent=False)
    field = req["field"]
    value = req["value"]

    try:
        user_config = ConfigModel.objects.filter(user=user).first()
        user_config[field] = value
        user_config.modified_at = datetime.now(timezone.utc).astimezone()
        user_config.save()

        config = get_user_config(user_config)

        res = jsonify({"status": "OK", "message": "User config has been updated successfully", "config": config})
        return res, 200, default_headers

    except:
        res = {"status": "Error", "message": "There was an error updating the user config"}
        return Response(json.dumps(res)), 400, default_headers


@user.route("/stocks_update", methods=["POST"])
def stocks_update():
    """
    Updates the user's stocks.
    """
    user = get_jwt_identity()
    req = request.get_json(force=True, silent=False)
    values = req["values"]

    try:
        user_data = DataModel.objects.filter(user=user).first()
        user_data["stocks"] = values
        user_data.modified_at = datetime.now(timezone.utc).astimezone()
        user_data.save()

        stocks = user_data.stocks

        res = jsonify({"status": "OK", "message": "User stocks have been updated successfully", "stocks": stocks})
        return res, 200, default_headers

    except:
        res = {"status": "Error", "message": "There was an error updating the user stocks"}
        return Response(json.dumps(res)), 400, default_headers


@user.route("/currencies_update", methods=["POST"])
def currencies_update():
    """
    Updates the user's currencies.
    """
    user = get_jwt_identity()
    req = request.get_json(force=True, silent=False)
    type = req["type"]
    from_symbol = req["from_symbol"]
    to_symbol = req["to_symbol"]

    try:
        user_data = DataModel.objects.filter(user=user).first()
        item = CurrencyDict(from_symbol=from_symbol, to_symbol=to_symbol)

        if type == "add":
            user_data.currencies.append(item)

        else:
            user_data.currencies.remove(item)

        user_data.modified_at = datetime.now(timezone.utc).astimezone()
        user_data.save()

        currencies = user_data.currencies

        res = jsonify({"status": "OK", "message": "User currencies have been updated successfully", "currencies": currencies})
        return res, 200, default_headers

    except:
        res = {"status": "Error", "message": "There was an error updating the user currencies"}
        return Response(json.dumps(res)), 400, default_headers


@user.route("/crypto_currencies_update", methods=["POST"])
def crypto_currencies_update():
    """
    Updates the user's cryptocurrencies.
    """
    user = get_jwt_identity()
    req = request.get_json(force=True, silent=False)
    type = req["type"]
    symbol = req["symbol"]
    market = req["market"]

    try:
        user_data = DataModel.objects.filter(user=user).first()
        item = CryptoCurrencyDict(symbol=symbol, market=market)

        if type == "add":
            user_data.crypto_currencies.append(item)

        else:
            user_data.crypto_currencies.remove(item)

        user_data.modified_at = datetime.now(timezone.utc).astimezone()
        user_data.save()

        crypto_currencies = user_data.crypto_currencies

        res = jsonify({"status": "OK", "message": "User cryptocurrencies have been updated successfully", "crypto_currencies": crypto_currencies})
        return res, 200, default_headers

    except:
        res = {"status": "Error", "message": "There was an error updating the user cryptocurrencies"}
        return Response(json.dumps(res)), 400, default_headers
