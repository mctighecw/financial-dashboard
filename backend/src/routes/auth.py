import json

from flask import request, Blueprint, Response, jsonify
from flask_cors import CORS
from flask_bcrypt import Bcrypt

from flask_jwt_extended import (create_access_token, create_refresh_token, jwt_required, \
    jwt_refresh_token_required, get_jwt_identity, set_access_cookies, set_refresh_cookies, \
    unset_jwt_cookies)

from src.models import (User as UserModel,
                        Config as ConfigModel,
                        Data as DataModel)

from src.helpers import get_user_info, get_user_config, get_user_financials
from src.constants import default_headers

bcrypt = Bcrypt()

auth = Blueprint("auth", __name__)
CORS(auth, resources=r"/api/auth/*")


@auth.before_request
def before_request():
    pass


@auth.route("/register", methods=["POST"])
def register():
    """
    Registers a new user and creates new database fields.
    """
    req = request.get_json(force=True, silent=False)

    if all(k in req for k in ["username", "email", "password"]):
        username = req["username"].lower()
        email = req["email"].lower()
        password = req["password"]

        exist_username = UserModel.objects.filter(username=username).first()
        exist_email = UserModel.objects.filter(email=email).first()

        if exist_username:
            res = {"status": "Error", "message": "Username is already taken"}
            return Response(json.dumps(res)), 409, default_headers

        elif exist_email:
            res = {"status": "Error", "message": "Email address is already taken"}
            return Response(json.dumps(res)), 409, default_headers

        else:
            password_hash = bcrypt.generate_password_hash(password).decode("utf-8")
            new_user = UserModel(
                username=username,
                email=email,
                password=password_hash
            )
            new_config = ConfigModel(user=username)
            new_data = DataModel(user=username)

            new_user.save()
            new_config.save()
            new_data.save()

            res = {"status": "OK", "message": "User has successfully registered"}
            return Response(json.dumps(res)), 200, default_headers
    else:
        res = {"status": "Error", "message": "Please provide all required fields"}
        return Response(json.dumps(res)), 400, default_headers


@auth.route("/login", methods=["POST"])
def login():
    """
    Logs the user in, then returns user config and stocks info.
    """
    req = request.get_json(force=True, silent=False)
    username = req["username"].lower()
    password = req["password"]
    current_user = UserModel.objects.filter(username=username).first()

    if current_user and bcrypt.check_password_hash(current_user.password, password):
        user_config = ConfigModel.objects.filter(user=username).first()
        user_data = DataModel.objects.filter(user=username).first()

        info = get_user_info(current_user)
        config = get_user_config(user_config)
        financials = get_user_financials(user_data)

        access_token = create_access_token(identity=username)
        refresh_token = create_refresh_token(identity=username)
        res = jsonify({"status": "OK", "message": "User logged in",
                "info": info, "config": config, "financials": financials
        })
        set_access_cookies(res, access_token)
        set_refresh_cookies(res, refresh_token)
        return res, 200, default_headers

    else:
        res = {"status": "Error", "message": "Wrong login credentials"}
        return Response(json.dumps(res)), 401, default_headers


@auth.route("/check", methods=["GET"])
@jwt_required
def check():
    """
    Checks if a user is still logged in.
    """
    try:
        username = get_jwt_identity()
        current_user = UserModel.objects.filter(username=username).first()
        user_config = ConfigModel.objects.filter(user=username).first()
        user_data = DataModel.objects.filter(user=username).first()

        info = get_user_info(current_user)
        config = config = get_user_config(user_config)
        financials = get_user_financials(user_data)

        res = jsonify({"status": "OK", "message": "User is still logged in",
                "info": info, "config": config, "financials": financials
        })
        return res, 200, default_headers

    except:
        res = {"status": "Error", "message": "No user is logged in"}
        return Response(json.dumps(res)), 401, default_headers


@auth.route("/refresh", methods=["POST"])
@jwt_refresh_token_required
def refresh():
    """
    Refreshes a valid JWT.
    """
    username = get_jwt_identity()
    access_token = create_access_token(identity=username)
    res = jsonify({"status": "OK", "message": "JWT refreshed"})
    set_access_cookies(res, access_token)
    return res, 200, default_headers


@auth.route("/logout", methods=["GET"])
def logout():
    """
    Logs the user out.
    """
    res = jsonify({"status": "OK", "message": "User logged out"})
    unset_jwt_cookies(res)
    return res, 200, default_headers
