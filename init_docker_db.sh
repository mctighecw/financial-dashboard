#!/bin/sh

echo Initializing database in container...

docker exec -it financial-dashboard-backend python ./src/db/init_db.py
