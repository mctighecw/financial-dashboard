# API Info

The Financial Dashboard uses the [Alpha Vantage](https://www.alphavantage.co) API to fetch current stock market, currency and cryptocurrency data. The backend, running a Flask web server, uses the [Alpha Vantage Python package](https://alpha-vantage.readthedocs.io/en/latest).

Data returned from the API is first filtered and sorted, then returned to the frontend to be parsed in order to be displayed correctly using the [Chart.js](https://www.chartjs.org) JavaScript library.

This file provides a brief overview of the original data format from _Alpha Vantage_ and the filtered/sorted format returned to the frontend, for reference.

## Alpha Vantage Raw Data

_General Notes:_

- The date/time keys are in reverse chronological order (newest to oldest)
- Data for multiple days is returned
- Note: example URLs need to have a valid key in place of `USER_KEY`

### Symbol Search API

Example input: `AAP`

URL:

`https://www.alphavantage.co/query?function=SYMBOL_SEARCH&keywords=AAP&apikey=USER_KEY`

Response:

```
{
  "bestMatches": [
    {
      "1. symbol": "AAP",
      "2. name": "Advance Auto Parts Inc.",
      "3. type": "Equity",
      "4. region": "United States",
      "5. marketOpen": "09:30",
      "6. marketClose": "16:00",
      "7. timezone": "UTC-05",
      "8. currency": "USD",
      "9. matchScore": "1.0000"
    },
    {
      "1. symbol": "AAPL",
      "2. name": "Apple Inc.",
      "3. type": "Equity",
      "4. region": "United States",
      "5. marketOpen": "09:30",
      "6. marketClose": "16:00",
      "7. timezone": "UTC-05",
      "8. currency": "USD",
      "9. matchScore": "0.8571"
    },
    ...
  ]
}
```

### Time Series

URL:

`http://www.alphavantage.co/query?function=TIME_SERIES_INTRADAY&symbol=DJI&interval=5min&outputsize=compact&apikey=USER_KEY`

Response:

```
{
  "Meta Data": {
    "1. Information": "Intraday (5min) open, high, low, close prices and volume",
    "2. Symbol": "DJI",
    "3. Last Refreshed": "2019-11-07 16:00:00",
    "4. Interval": "5min",
    "5. Output Size": "Compact",
    "6. Time Zone": "US/Eastern"
  },
  "Time Series (5min)": {
    "2019-11-07 16:00:00": {
      "1. open": "27655.9400",
      "2. high": "27682.9800",
      "3. low": "27649.7900",
      "4. close": "27678.5100",
      "5. volume": "14572806"
    },
    "2019-11-07 15:55:00": {
      "1. open": "27678.4805",
      "2. high": "27684.8691",
      "3. low": "27649.8301",
      "4. close": "27656.6699",
      "5. volume": "6050733"
    },
    ...
  }
}
```

### Foreign Exchange

URL:

`http://www.alphavantage.co/query?function=FX_DAILY&from_symbol=USD&to_symbol=EUR&outputsize=compact&apikey=USER_KEY`

Response:

```
{
  "Meta Data": {
    "1. Information": "Forex Daily Prices (open, high, low, close)",
    "2. From Symbol": "USD",
    "3. To Symbol": "EUR",
    "4. Output Size": "Compact",
    "5. Last Refreshed": "2019-11-08 09:55:00",
    "6. Time Zone": "UTC"
  },
  "Time Series FX (Daily)": {
    "2019-11-08": {
      "1. open": "0.9044",
      "2. high": "0.9055",
      "3. low": "0.9042",
      "4. close": "0.9055"
    },
    "2019-11-07": {
      "1. open": "0.9032",
      "2. high": "0.9058",
      "3. low": "0.9013",
      "4. close": "0.9048"
    },
    ...
  }
}
```

### Cryptocurrencies

URL:

`http://www.alphavantage.co/query?function=DIGITAL_CURRENCY_DAILY&symbol=ETH&market=EUR&apikey=USER_KEY`

Response:

```
{
  "Meta Data": {
    "1. Information": "Daily Prices and Volumes for Digital Currency",
    "2. Digital Currency Code": "ETH",
    "3. Digital Currency Name": "Ethereum",
    "4. Market Code": "EUR",
    "5. Market Name": "Euro",
    "6. Last Refreshed": "2019-11-08 00:00:00",
    "7. Time Zone": "UTC"
  },
  "Time Series (Digital Currency Daily)": {
    "2019-11-08": {
      "1a. open (EUR)": "168.89901600",
      "1b. open (USD)": "186.67000000",
      "2a. high (EUR)": "170.33764800",
      "2b. high (USD)": "188.26000000",
      "3a. low (EUR)": "168.47376000",
      "3b. low (USD)": "186.20000000",
      "4a. close (EUR)": "169.75857600",
      "4b. close (USD)": "187.62000000",
      "5. volume": "15249.35365000",
      "6. market cap (USD)": "15249.35365000"
    },
    "2019-11-07": {
      "1a. open (EUR)": "172.96156800",
      "1b. open (USD)": "191.16000000",
      "2a. high (EUR)": "173.96589600",
      "2b. high (USD)": "192.27000000",
      "3a. low (EUR)": "167.01703200",
      "3b. low (USD)": "184.59000000",
      "4a. close (EUR)": "168.90806400",
      "4b. close (USD)": "186.68000000",
      "5. volume": "309882.08206000",
      "6. market cap (USD)": "309882.08206000"
    },
    ...
  }
}
```

### Technical Indicators

URL:

`http://www.alphavantage.co/query?function=AROON&symbol=BA&interval=5min&time_period=20&series_type=close&apikey=USER_KEY`

Response:

```
{
  "Meta Data": {
    "1: Symbol": "BA",
    "2: Indicator": "Aroon (AROON)",
    "3: Last Refreshed": "2019-11-07 16:00:00",
    "4: Interval": "5min",
    "5: Time Period": 20,
    "6: Time Zone": "US/Eastern Time"
  },
  "Technical Analysis: AROON": {
    "2019-11-07 16:00": {
      "Aroon Up": "5.0000",
      "Aroon Down": "90.0000"
    },
    "2019-11-07 15:55": {
      "Aroon Up": "0.0000",
      "Aroon Down": "95.0000"
    },
    ...
  }
}
```

## Filtered/Sorted Data

_General Notes:_

- Backend receives the raw data above, processes it, then returns the following to the frontend
- Data undergo some filtering to remove extra values (e.g. unnecessary days)
- Dictionary keys are simplified
- Values are sorted chronologically (oldest to newest)

### Symbol Search API

Response:

```
{
  "status": "OK",
  "message": "Stock symbols data retrieved successfully",
  "search_data": [
    {
      "symbol": "AAP",
      "name": "Advance Auto Parts Inc."
    },
    {
      "symbol": "AAPL",
      "name": "Apple Inc."
    },
    ...
  ]
}
```

### Time Series

Response:

```
{
  "status": "OK",
  "message": "Time series data retrieved successfully",
  "date": "2019-11-07",
  "meta_data": {
    "1. Information": "Intraday (5min) open, high, low, close prices and volume",
    "2. Symbol": "DJI",
    "3. Last Refreshed": "2019-11-07 16:00:00",
    "4. Interval": "5min",
    "5. Output Size": "Compact",
    "6. Time Zone": "US/Eastern"
  },
  "data": {
    "2019-11-07 09:35:00": {
      "line": {
        "open": "27590.1602",
        "high": "27666.5508",
        "low": "27590.1602",
        "close": "27644.8906"
      },
      "bar": {
        "volume": "0"
      }
    },
    "2019-11-07 09:40:00": {
      "line": {
        "open": "27643.8008",
        "high": "27673.4609",
        "low": "27642.0293",
        "close": "27666.6094"
      },
      "bar": {
        "volume": "4302437"
      }
    },
    ...
  }
}
```

### Foreign Exchange

Response:

```
{
  "status": "OK",
  "message": "Foreign exchange data retrieved successfully",
  "meta_data": {
    "1. Information": "Forex Daily Prices (open, high, low, close)",
    "2. From Symbol": "USD",
    "3. To Symbol": "EUR",
    "4. Output Size": "Compact",
    "5. Last Refreshed": "2019-11-08 10:30:00",
    "6. Time Zone": "UTC"
  },
  "data": {
    "2019-07-16": {
      "open": "0.8878",
      "high": "0.8924",
      "low": "0.8874",
      "close": "0.8917"
    },
    "2019-07-17": {
      "open": "0.8918",
      "high": "0.8926",
      "low": "0.8900",
      "close": "0.8905"
    },
    ...
  }
}
```

### Cryptocurrencies

Response:

```
{
  "status": "OK",
  "message": "Cryptocurrencies data retrieved successfully",
  "meta_data": {
    "1. Information": "Daily Prices and Volumes for Digital Currency",
    "2. Digital Currency Code": "ETH",
    "3. Digital Currency Name": "Ethereum",
    "4. Market Code": "EUR",
    "5. Market Name": "Euro",
    "6. Last Refreshed": "2019-11-08 00:00:00",
    "7. Time Zone": "UTC"
  },
  "data": {
    "2017-08-17": {
      "open": "272.46242400",
      "high": "282.46046400",
      "low": "269.63040000",
      "close": "273.24960000"
    },
    "2017-08-18": {
      "open": "273.24960000",
      "high": "282.10759200",
      "low": "256.90891200",
      "close": "265.97500800"
    },
    ...
  }
}
```

### Technical Indicators

Response:

```
{
  "status": "OK",
  "message": "Tech indicators data retrieved successfully",
  "date": "2019-11-07",
  "data": {
    "Aroon": {
      "meta_data": {
        "1: Symbol": "BA",
        "2: Indicator": "Aroon (AROON)",
        "3: Last Refreshed": "2019-11-07 16:00:00",
        "4: Interval": "5min",
        "5: Time Period": 20,
        "6: Time Zone": "US/Eastern Time"
      },
      "data": {
        "2019-11-07 09:35": {
          "Aroon Up": "100.0000",
          "Aroon Down": "90.0000"
        },
        "2019-11-07 09:40": {
          "Aroon Up": "100.0000",
          "Aroon Down": "85.0000"
        },
        ...
      }
    }
  }
}
```
